import lombok.extern.slf4j.Slf4j;
import me.friwi.jcefmaven.CefAppBuilder;
import me.friwi.jcefmaven.CefInitializationException;
import me.friwi.jcefmaven.MavenCefAppHandlerAdapter;
import me.friwi.jcefmaven.UnsupportedPlatformException;
import me.friwi.jcefmaven.impl.progress.ConsoleProgressHandler;
import org.cef.CefApp;

import java.io.File;
import java.io.IOException;

@Slf4j
public class Main {
    public static void main(String[] args) throws UnsupportedPlatformException, CefInitializationException, IOException, InterruptedException {
        log.debug("处理过程中");
//        Ant ant = new Ant();
//        ant.addConfiguredTarget(new Ant.TargetElement());
//        ant.execute();
        //Create a new CefAppBuilder instance
        CefAppBuilder builder = new CefAppBuilder();

//Configure the builder instance
        builder.setInstallDir(new File("jcef-bundle")); //Default
        builder.setProgressHandler(new ConsoleProgressHandler()); //Default
        builder.addJcefArgs("--disable-gpu"); //Just an example
        builder.getCefSettings().windowless_rendering_enabled = true; //Default - select OSR mode

//Set an app handler. Do not use CefApp.addAppHandler(...), it will break your code on MacOSX!
        builder.setAppHandler(new MavenCefAppHandlerAdapter() {

        });

//Build a CefApp instance using the configuration above
        CefApp app = builder.build();
        app.createClient();
    }

}
