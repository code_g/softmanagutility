package org.groupg.project.Utils;

import lombok.Data;

/**
 * 配置文件的相关信息
 */
@Data
public class ConfigFileModel {

        /**
         * 唯一识别号
         */
        private String id;
        /**
         * 名称
         */
        private String name;
        /**
         * 配置文件的路径，根据配置文件读取可以获取对应配置文件的内容
         */
        private String configFilePath;

        /**
         * 配置文件的类型
         */
        private String type = ConfigFileType.ANT;

    }