package org.groupg.project.Utils;

/**
 * 配置文件类型
 */
public interface ConfigFileType {
    /**
     * ant 脚本的枚举
     */
    String ANT = "ANT";
    /**
     * PMD 脚本的枚举
     */
    String PMD = "PMD";
    /**
     * FLYWAY 脚本的枚举
     */
    String FLYWAY = "FLYWAY";
}
