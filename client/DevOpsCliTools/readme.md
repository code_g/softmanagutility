# DevOpsCliTools

## 项目说明

是一个简单版的自动化测试（接口、UI）、集成、静态代码检查、数据库版本管理的工具项目

## 依赖

jdk 17

gradle 8 

## 项目目录结构

```
├─.gradle   // gradle 本地编译目录
├─.idea     // idea 项目文件夹
├─build     // 编译输出目录
├─docs      // 文档目录
├─gradle    // gradle 文件配置目录
└─src       // 源码目录
    ├─main
    │  ├─java
    │  └─resources
    │      ├─antBuilds  // ant 脚本目录
    │      ├─pmdConfigs // pmd 配置脚本目录
    │      └─SQLBuilds  // sql 脚本配置目录
    └─test              // 测试源码目录
```

目录详细内容请使用 docs 文档目录中的使用手册等相关资料。 