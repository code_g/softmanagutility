module org.groupg.softmanageutility.softmanageutility {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires transitive org.slf4j;
    requires cn.hutool;
    requires transitive com.flexganttfx.view;
    requires transitive com.calendarfx.view;
    requires transitive com.flexganttfx.core;
    requires transitive com.flexganttfx.extras;
    requires transitive com.flexganttfx.model;
    requires org.apache.sshd.common;
    requires org.apache.commons.logging;
    requires org.jooq.jool;
//    requires org.apache.commons.logging;
////    requires jsch.agentproxy.svnkit.trilead.ssh2;
////    requires jsch.agentproxy.pageant;
//    requires org.apache.commons.pool2;
////    requires sshd.common;
//    requires openjpa.all;
//    requires commons.dbcp2;

//    opens org.apache.commons.dbcp2 to openjpa.all;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
//    requires com.almasb.fxgl.all;
//    requires kotlin.stdlib;
//    requires kotlin.reflect;

    opens org.groupg.softmanageutility.softmanageutility to javafx.fxml;
    exports org.groupg.softmanageutility.softmanageutility;
}