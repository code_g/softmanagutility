package org.groupg.softmanageutility.softmanageutility;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.groupg.softmanageutility.softmanageutility.main.view.MainStage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
//        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
//        Scene scene = new Scene(fxmlLoader.load(), 1600, 900);
//        stage.setTitle("SoftManageUtility");
//        stage.setScene(scene);
//        stage.show();
        stage = new MainStage();
    }

    public static void main(String[] args) {
        launch();
    }
}