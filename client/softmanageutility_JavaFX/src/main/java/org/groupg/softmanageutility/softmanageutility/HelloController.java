package org.groupg.softmanageutility.softmanageutility;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloController {

    static final Logger log = LoggerFactory.getLogger(HelloController.class);
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        log.debug("setText");
        welcomeText.setText("Welcome to JavaFX Application!");
    }
}