package org.groupg.softmanageutility.softmanageutility.main.view;

import javafx.stage.Stage;

/**
 * 页面默认展示的首页，也是默认的 UI 现成页
 */
public class MainStage extends Stage {

    private static MainStage mainStage = null;

    public static synchronized MainStage getMainStage(){
        if (mainStage == null){
            mainStage = new MainStage();
        }
        return mainStage;
    }

    private static MainStage createDefaultMainStage(){
        MainStage mainStage1 = new MainStage();
        mainStage1.setTitle("开发工具箱");
        mainStage1.setWidth(1600);
        mainStage1.setHeight(900);
        return mainStage1;
    }

}
