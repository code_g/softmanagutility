package org.groupg.project.docvarinit.utils;

import org.apache.commons.text.StrSubstitutor;
import org.apache.commons.text.StringSubstitutor;
import org.groupg.project.docvarinit.model.FileProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TextUtils {

    public static String replaceText(List<FileProperty> fileProperties , String text) {
        for (FileProperty fileProperty : fileProperties) {
            text = replaceText(fileProperty, text);
        }
        return text;
    }

    public static String replaceText(FileProperty fileProperty, String text) {
        return text.replaceAll("\\$\\{"+fileProperty.propertyNameProperty().getValue()+"}", fileProperty.propertyValueProperty().getValue());
    }

    public static String replaceTextByCommons(List<FileProperty> fileProperties, String text) {
        Map<String ,String> dataMap = new HashMap<>();
        for (FileProperty fileProperty : fileProperties) {
            dataMap.put(fileProperty.propertyNameProperty().getValue(), fileProperty.propertyValueProperty().getValue());
        }
        StringSubstitutor substitutor = new StringSubstitutor(dataMap);

        return substitutor.replace(text);
    }
}
