package org.groupg.project.docvarinit;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.groupg.project.docvarinit.model.Person;

public class EditableTableViewExample extends Application {

    @Override
    public void start(Stage stage) {
        // 创建数据
        ObservableList<Person> data = FXCollections.observableArrayList(
                new Person("John Doe", "john.doe@example.com"),
                new Person("Jane Doe", "jane.doe@example.com")
        );

        // 创建TableView
        TableView<Person> table = new TableView<>();
        table.setEditable(true);

        // 创建列
        TableColumn<Person, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(100);
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        TableColumn<Person, String> emailColumn = new TableColumn<>("Email");
        emailColumn.setMinWidth(200);
        emailColumn.setCellValueFactory(cellData -> cellData.getValue().emailProperty());
        emailColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        // 添加列到TableView
        table.getColumns().add(nameColumn);
        table.getColumns().add(emailColumn);

        // 设置数据
        table.setItems(data);

        // 创建场景和舞台
        VBox vBox = new VBox(table);
        Scene scene = new Scene(vBox);
        stage.setScene(scene);
        stage.setTitle("Editable TableView Example");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}