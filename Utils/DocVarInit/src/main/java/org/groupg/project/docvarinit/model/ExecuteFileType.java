package org.groupg.project.docvarinit.model;

import org.apache.tika.mime.MimeType;

import java.io.File;
import java.util.List;

public enum ExecuteFileType {

    PPT {
        @Override
        public List<FileProperty> getAllFileProperty(File file) {
            return List.of();
        }
    };

    public abstract List<FileProperty> getAllFileProperty(File file);
}
