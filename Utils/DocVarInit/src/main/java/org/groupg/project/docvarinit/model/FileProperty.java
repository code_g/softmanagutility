package org.groupg.project.docvarinit.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.StringJoiner;

public class FileProperty {
    private final StringProperty propertyName = new SimpleStringProperty();
    private final StringProperty propertyValue = new SimpleStringProperty();

    public FileProperty(String propertyName, String propertyValue) {
        this.propertyName.set(propertyName);
        this.propertyValue.set(propertyValue);
    }

    public StringProperty propertyNameProperty() {
        return propertyName;
    }

    public StringProperty propertyValueProperty() {
        return propertyValue;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", FileProperty.class.getSimpleName() + "[", "]")
                .add("propertyName=" + propertyName.getValue())
                .add("propertyValue=" + propertyValue.getValue())
                .toString();
    }
}
