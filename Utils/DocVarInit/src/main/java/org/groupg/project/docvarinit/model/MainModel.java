package org.groupg.project.docvarinit.model;

import org.apache.commons.text.StringSubstitutor;
import org.groupg.project.docvarinit.utils.MainFileUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MainModel {
    private File sourceDir;
    private File outputDir;
    private List<File> files;
    private List<FileProperty> fileProperties;
    private StringSubstitutor substitutor;

    public MainModel(File sourceDir, File outputDir , List<File> files , List<FileProperty> fileProperties) {
        this.sourceDir = sourceDir;
        this.outputDir = outputDir;
        this.files = MainFileUtils.listFiles(sourceDir);
        this.fileProperties = fileProperties;
        Map<String ,String> dataMap = new HashMap<>();
        for (FileProperty fileProperty : fileProperties) {
            dataMap.put(fileProperty.propertyNameProperty().getValue(), fileProperty.propertyValueProperty().getValue());
        }
        this.substitutor = new StringSubstitutor(dataMap);

    }
    public MainModel() {

    }

    public File getSourceDir() {
        return sourceDir;
    }

    public void setSourceDir(File sourceDir) {
        this.sourceDir = sourceDir;
        this.files = MainFileUtils.listFiles(sourceDir);
    }

    public File getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(File outputDir) {
        this.outputDir = outputDir;
    }

    public List<File> getFiles() {
        return files;
    }

    public List<FileProperty> getFileProperties() {
        return fileProperties;
    }

    public void setFileProperties(List<FileProperty> fileProperties) {
        this.fileProperties = fileProperties;
        Map<String ,String> dataMap = new HashMap<>();
        for (FileProperty fileProperty : fileProperties) {
            dataMap.put(fileProperty.propertyNameProperty().getValue(), fileProperty.propertyValueProperty().getValue());
        }
        this.substitutor = new StringSubstitutor(dataMap);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MainModel mainModel = (MainModel) o;
        return Objects.equals(getSourceDir(), mainModel.getSourceDir()) && Objects.equals(getOutputDir(), mainModel.getOutputDir()) && Objects.equals(getFiles(), mainModel.getFiles()) && Objects.equals(getFileProperties(), mainModel.getFileProperties());
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(getSourceDir());
        result = 31 * result + Objects.hashCode(getOutputDir());
        result = 31 * result + Objects.hashCode(getFiles());
        result = 31 * result + Objects.hashCode(getFileProperties());
        return result;
    }

    public StringSubstitutor getSubstitutor() {
        return substitutor;
    }
}
