package org.groupg.project.docvarinit.utils;

import javafx.scene.control.Alert;

public class AlertUtil {
    static final Alert errorAlert = new Alert(Alert.AlertType.ERROR);
    static final Alert warningAlert = new Alert(Alert.AlertType.WARNING);
    static final Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
    static final Alert conAlert = new Alert(Alert.AlertType.CONFIRMATION);

    public static void error(String headerText, String contentText){
        errorAlert.setTitle("错误！");
        errorAlert.setHeaderText(headerText);
        errorAlert.setContentText(contentText);
        errorAlert.showAndWait();
    }

    public static void warning(String headerText, String contentText){
        warningAlert.setTitle("警告！");
        warningAlert.setHeaderText(headerText);
        warningAlert.setContentText(contentText);
        warningAlert.showAndWait();
    }
}
