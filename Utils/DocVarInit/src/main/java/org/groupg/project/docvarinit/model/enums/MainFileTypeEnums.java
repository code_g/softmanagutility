package org.groupg.project.docvarinit.model.enums;

import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import org.groupg.project.docvarinit.model.FileProperty;
import org.groupg.project.docvarinit.model.MainModel;

import java.io.*;
import java.util.List;

public enum MainFileTypeEnums {

    TEXT {
        @Override
        public void coverFile(File sourceFile, File targetFile, MainModel model) throws IOException {

            if (targetFile.exists()) {
                targetFile.delete();
                targetFile.getParentFile().mkdirs();
                targetFile.createNewFile();
            }
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(sourceFile));
                 BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(targetFile))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String processedLine = model.getSubstitutor().replace(line);
                    bufferedWriter.write(processedLine);
                    bufferedWriter.newLine();
                }

                System.out.println("文件处理完成。");
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        @Override
        public List<String> getSettingFileType() {
            return List.of("txt", "md", "markdown", "asciidoc", "adoc", "puml", "json", "java", "ts", "jsx", "js", "xml", "properties", "gradle", "gradle");
        }
    },
    PPT {
        @Override
        public void coverFile(File sourceFile, File targetFile, MainModel model) {

        }

        @Override
        public List<String> getSettingFileType() {
            return List.of("pptx", "ppt");
        }

    },
    EXCEL {
        @Override
        public void coverFile(File sourceFile, File targetFile, MainModel model) {

        }

        @Override
        public List<String> getSettingFileType() {
            return List.of("xls", "xlsx");
        }

    },
    WORD {
        @Override
        public void coverFile(File sourceFile, File targetFile, MainModel model) {

        }

        @Override
        public List<String> getSettingFileType() {
            return List.of("doc", "docs");
        }

    },
    PROJECT {
        @Override
        public void coverFile(File sourceFile, File targetFile, MainModel model) {

        }

        @Override
        public List<String> getSettingFileType() {
            return List.of("mpp");
        }

    },
    VISIO {
        @Override
        public void coverFile(File sourceFile, File targetFile, MainModel model) {

        }

        @Override
        public List<String> getSettingFileType() {
            return List.of("vsd", "vsdx");
        }

    },
    OTHER {
        @Override
        public void coverFile(File sourceFile, File targetFile, MainModel model) {
            FileUtil.copy(sourceFile,targetFile,true);
        }

        @Override
        public List<String> getSettingFileType() {
            return List.of("other");
        }

    };

    public abstract void coverFile(File sourceFile, File targetFile, MainModel model) throws IOException;

    public abstract List<String> getSettingFileType();


    public static MainFileTypeEnums getFileType(File sourceFile) {
        String fileType = FileTypeUtil.getType(sourceFile).toLowerCase();

        if (PPT.getSettingFileType().contains(fileType)) {
            return PPT;
        }
        return OTHER;
    }

    static final Log log = LogFactory.get();
}
