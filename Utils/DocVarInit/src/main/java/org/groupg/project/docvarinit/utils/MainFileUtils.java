package org.groupg.project.docvarinit.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainFileUtils {

        public static List<File> listFiles(File file) {
        List<File> paths = new ArrayList<>();
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    paths.addAll(listFiles(f)); // 递归调用，并将结果添加到列表中
                }
            }
        } else {
            paths.add(file);
        }
        return paths;
    }
}
