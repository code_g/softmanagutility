module org.groupg.project.docvarinit {
    requires javafx.controls;
    requires javafx.fxml;

//    requires org.controlsfx.controls;
//    requires com.dlsc.formsfx;
//    requires net.synedra.validatorfx;
//    requires org.kordamp.bootstrapfx.core;
    requires cn.hutool;
    requires org.apache.tika.core;
    requires java.xml;
    requires org.apache.commons.io;
    requires org.slf4j;
    requires org.apache.commons.logging;
    requires org.apache.poi.poi;
    requires org.apache.poi.ooxml;
    requires org.apache.poi.ooxml.schemas;
    requires org.apache.poi.scratchpad;
    requires org.apache.commons.text;


    opens org.groupg.project.docvarinit to javafx.fxml;
    exports org.groupg.project.docvarinit;
}