# SoftManagUtility

## 研发原则介绍

> 使用者定义：源代码使用者和软件使用者

1. 考虑使用者技术能力范围，尽量用他们会的或者他们应该会的。
2. 软件应尽可能的少占用内存和cpu。
3. 文档与代码一体化，文档在代码上也在软件上。
4. 软件应支持在线和离线的方式同时使用。
5. 在线应用的服务端应尽可能避免不同数据库带来的SQL方言问题。
6. 上面的几点如果有冲突则优先保证序号大的规则。

## 软件架构

./client 软件客户端

./docs 软件说明文档

./server 服务器端


## 安装教程（还未完成）

1.  xxxx
2.  xxxx
3.  xxxx

## 使用说明（还未完成）

1.  xxxx
2.  xxxx
3.  xxxx

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
