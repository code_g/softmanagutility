package org.groupg.softUtilities.ProjectDaily.ReimbursementManagement;

import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.DateType;
import xyz.erupt.annotation.sub_field.sub_edit.NumberType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;


@Erupt(name = "报销单项目-差旅项目清单"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_reimbursement_items_travel_ins")    //数据库表名
@Entity
@Getter
@Setter
public class ReimbursementItemTravels extends HyperModelVo {

    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
        edit = @Edit(title = "起止时间及地点", type = EditType.DIVIDE)
    )
    private String divide01;
    /**
     * 起点
     */
    @EruptField(
            views = @View(title = "起点"),
            edit = @Edit(title = "起点", search = @Search(vague = true))
    )
    private String startingPoint;

    @EruptField(
        views = @View(title = "出发时间"),
        edit = @Edit(title = "出发时间",
                dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                , search = @Search))
    @Column(name = "startDay")
    private Date startDay;

    /**
     * 终点
     */
    @EruptField(
            views = @View(title = "终点"),
            edit = @Edit(title = "终点", search = @Search(vague = true))
    )
    private String endPoint;
    /**
     * 结束时间
     */
    @EruptField(
        views = @View(title = "结束时间"),
        edit = @Edit(title = "结束时间",
                dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                , search = @Search))
    @Column(name = "endDay")
    private Date endDay;

    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
        edit = @Edit(title = "交通费", type = EditType.DIVIDE)
    )
    private String divide02;
    /**
     * 交通工具
     */
    @EruptField(
            views = @View(title = "交通工具"),
        edit = @Edit(title = "交通工具")
    )
    private String transportation;

    /**
     * 单据张数
     */
    @EruptField(
    edit = @Edit(title = "单据张数", numberType = @NumberType(min = 0,max = 100))
    )
    private Integer docNumber;

    /**
     * 金额
     */
    @EruptField(
        edit = @Edit(title = "金额", numberType = @NumberType)
    )
    private Double tranMount;

    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
        edit = @Edit(title = "出差补贴", type = EditType.DIVIDE)
    )
    private String divide03;

    /**
     * 人数
     */
    @EruptField(
    edit = @Edit(title = "人数", numberType = @NumberType(min = 0,max = 100))
    )
    private Integer personNumber = 1;

    /**
     * 天数
     */
    @EruptField(
    edit = @Edit(title = "天数", numberType = @NumberType(min = 0))
    )
    private Integer dayNumber = 1;


    /**
     * 补贴标准
     */
    @EruptField(
        edit = @Edit(title = "补贴标准", numberType = @NumberType)
    )
    private Double standardsMount;

    /**
     * 金额
     */
    @EruptField(
        edit = @Edit(title = "金额", numberType = @NumberType)
    )
    private Double travelMount;


    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
        edit = @Edit(title = "其他", type = EditType.DIVIDE)
    )
    private String divide04;

    /**
     * 费用项目
     */
    @EruptField(
            views = @View(title = "费用项目"),
            edit = @Edit(title = "费用项目", search = @Search(vague = true))
    )
    private String expenseName;

    /**
     * 项目金额
     */
    @EruptField(
        edit = @Edit(title = "项目金额", numberType = @NumberType)
    )
    private Double expenseMount;

}
