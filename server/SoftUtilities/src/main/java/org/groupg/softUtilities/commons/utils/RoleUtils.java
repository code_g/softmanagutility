package org.groupg.softUtilities.commons.utils;

import org.springframework.stereotype.Component;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.upms.model.EruptMenu;
import xyz.erupt.upms.model.EruptRole;
import xyz.erupt.upms.model.EruptUserByRoleView;

import javax.annotation.Resource;
import java.util.Set;

@Component
public class RoleUtils {
    @Resource
    private EruptDao eruptDao;

    public void createOrReplaceRole(boolean status, String name, String code , Set<EruptMenu> menus ,Set<EruptUserByRoleView> users){
        EruptRole eruptRole = new EruptRole();
        eruptRole.setName(name);
        eruptRole.setStatus(status);
        eruptRole.setCode(code);
        eruptRole.setMenus(menus);
        eruptRole.setUsers(users);
        eruptDao.persistIfNotExist(EruptRole.class, eruptRole, "code", eruptRole.getCode());
    }


}
