package org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.utils;

import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.repository.PersonInofRepository;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.repository.ProjectRepository;
import org.groupg.softUtilities.commons.utils.UserTools;
import org.springframework.stereotype.Service;
import xyz.erupt.core.exception.EruptApiErrorTip;

import javax.annotation.Resource;
import java.util.List;

@Service("ProjectUtils")
public class ProjectUtils {

    @Resource
    ProjectRepository projectRepository;
    @Resource
    PersonInofRepository personInofRepository;

    @Resource
    UserTools userTools;

    /**
     * 获取该用户的所在的项目（所有项目的集合）
     *
     * @return
     */
    public List<ProjectInfo> getCurrentUserProjects() {
        return projectRepository.findAllByProjectManager(userTools.getCurrentEruptUser().getAccount());
    }

    /**
     * 获取该用户的所在的项目（如果有多个就拿第一个）
     *
     * @return
     */
    public ProjectInfo getCurrentUserProject() {
        List<ProjectInfo> projectInfos = projectRepository.findAllByProjectManager(userTools.getCurrentEruptUser().getAccount());
        if (projectInfos.isEmpty()) throw new EruptApiErrorTip("你不是项目的项目经理，请确认是否做好关联！");
        return projectInfos.get(0);
    }


    /**
     * 获取该用户的所在的项目（如果有多个就拿第一个）
     *
     * @return
     */
    public Long getCurrentUserProjectID() {
        List<PersonInfo> personInfos = personInofRepository.findAllByUser(userTools.getCurrentEruptUser().getAccount());
        if (personInfos.isEmpty())
            throw new EruptApiErrorTip("你当前并没有在任何项目下，请与你的管理员联系并配置好项目！");
        return personInfos.get(0).getProjectInfos().iterator().next().getId();
    }


}
