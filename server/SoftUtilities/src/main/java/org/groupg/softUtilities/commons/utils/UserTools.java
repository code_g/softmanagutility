package org.groupg.softUtilities.commons.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.erupt.core.util.MD5Util;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.upms.model.EruptUser;
import xyz.erupt.upms.service.EruptContextService;
import xyz.erupt.upms.service.EruptUserService;

import javax.annotation.Resource;
import java.util.Date;

@Component
public class UserTools {

    @Resource
    private EruptUserService eruptUserService;

    @Resource
    private EruptContextService eruptContextService;

    @Resource
    private EruptDao eruptDao;

    public EruptUser getCurrentEruptUser() {
        return eruptUserService.getCurrentEruptUser();
    }

    /**
     * 创建并合并用户，如果已存在则会被覆盖，覆盖的标准是 account
     * @param isAdmin 是否为管理员
     * @param status 状态是否正常
     * @param account 登录用户名
     * @param password 密码
     * @param name 名称
     */
    public void createOrReplaceUser(boolean isAdmin,boolean status, String account,String password,String name){
        EruptUser eruptUser = new EruptUser();
        eruptUser.setIsAdmin(isAdmin);
        eruptUser.setIsMd5(true);
        eruptUser.setStatus(status);
        eruptUser.setCreateTime(new Date());
        eruptUser.setAccount(account);
        eruptUser.setPassword(MD5Util.digest(password));
        eruptUser.setName(name);
        eruptDao.persistIfNotExist(EruptUser.class, eruptUser, "account", eruptUser.getAccount());
}

}
