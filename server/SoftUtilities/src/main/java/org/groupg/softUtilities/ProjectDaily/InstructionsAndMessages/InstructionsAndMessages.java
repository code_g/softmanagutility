package org.groupg.softUtilities.ProjectDaily.InstructionsAndMessages;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.DateType;
import xyz.erupt.annotation.sub_field.sub_edit.HtmlEditorType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 嘱咐与寄语主要是用于人员在上下班签到时提供的一个提示由项目经理技术经理以及自己设置，可以做到一个类似待办的效果。
 */
@Erupt(name = "嘱咐与寄语配置"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_instructionsandmessages_ins")    //数据库表名
@Entity
@Getter
@Setter
public class InstructionsAndMessages extends HyperModelVo {

    /**
     * 寄语的起止时间
     */
    @EruptField(
            views = @View(title = "起始时间"),
            edit = @Edit(title = "起始时间",
                    dateType = @DateType(type = DateType.Type.DATE, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date startDay;

    @EruptField(
            views = @View(title = "截止时间"),
            edit = @Edit(title = "截止时间",
                    dateType = @DateType(type = DateType.Type.DATE, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date endDay;
    /**
     * 寄语的作用人
     */
    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_person_instructions_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "risk_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "person_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "寄语的作用人",desc = "项目成员管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<PersonInfo> personInfos;

    /**
     * 寄语的内容
     */
    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "寄语", type = ViewType.HTML, export = false),
            edit = @Edit(title = "寄语",
                    type = EditType.HTML_EDITOR,
                    htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String riskContent;

}
