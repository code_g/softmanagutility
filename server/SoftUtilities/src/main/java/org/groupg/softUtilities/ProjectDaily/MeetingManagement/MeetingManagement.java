package org.groupg.softUtilities.ProjectDaily.MeetingManagement;


import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Erupt(name = "会议管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_meetingmanagement_ins")    //数据库表名
@Entity
@Getter
@Setter
public class MeetingManagement extends HyperModelVo {
    /**
     * 会议主旨
     */
    @EruptField(
            views = @View(title = "会议主旨"),
            edit = @Edit(title = "会议主旨", search = @Search(vague = true))
    )
    private String name;

    /**
     * 参会人员
     */
    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "用户名", column = "account"),
                    @View(title = "姓名", column = "name")
            },
            edit = @Edit(title = "报销人", desc = "注意：需要先在用户中心完成用户的新增之后才能在这里使用用户，报销人与出差人是一个人", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType()
            )
    )
    private PersonInfo personInfo;
    /**
     * 会议时间
     */
    @EruptField(
        views = @View(title = "开始时间"),
        edit = @Edit(title = "开始时间",
                dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                , search = @Search))
    private Date startDay;
    @EruptField(
        views = @View(title = "结束时间"),
        edit = @Edit(title = "结束时间",
                dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                , search = @Search))
    private Date endDay;

    /**
     * 会议类型
     */
    @EruptField(
            views = @View(title = "会议类型"),
            edit = @Edit(title = "会议类型", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "0", label = "线下")
                            , @VL(value = "1", label = "线上")
                            , @VL(value = "2", label = "混合")
                    })
                    , search = @Search))
    private String meetingType = "0";

    /**
     * 会议议程
     */
    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "会议议程", type = ViewType.HTML),
            edit = @Edit(title = "会议议程",
                     type = EditType.HTML_EDITOR,
                     htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String agenda;

    /**
     * 资料上传
     */
    @EruptField(
        views = @View(title = "资料上传"),
        edit = @Edit(title = "资料上传", type = EditType.ATTACHMENT,
                     attachmentType = @AttachmentType)
    )
    private String attachment;

    /**
     * 会议现场
     */
    @Lob
    @EruptField(
    views = @View(title = "会议现场"),
    edit = @Edit(title = "会议现场", type = EditType.TEXTAREA,search = @Search(vague = true))
)
    private String path;

    /**
     * 会议简单说明
     */
    @Lob
    @EruptField(
    views = @View(title = "会议简单说明"),
    edit = @Edit(title = "会议简单说明", type = EditType.TEXTAREA,search = @Search(vague = true))
)
    private String remark;

    /**
     * 会议纪要
     */
    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "会议纪要", type = ViewType.HTML),
            edit = @Edit(title = "会议纪要",
                     type = EditType.HTML_EDITOR,
                     htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String meetingMinutes;
}
