package org.groupg.softUtilities.ProjectConfiguration.PersonInfo.utils;

import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.repository.PersonInofRepository;
import org.groupg.softUtilities.commons.utils.UserTools;
import org.springframework.stereotype.Service;
import xyz.erupt.core.exception.EruptApiErrorTip;

import javax.annotation.Resource;
import java.util.List;

@Service("PersonInfoUtils")
public class PersonInfoUtils {

    @Resource
    PersonInofRepository personInofRepository;

    @Resource
    UserTools userTools;

    /**
     * 获取该用户的所在的项目（所有项目的集合）
     *
     * @return 列表对象 用户的基本情况列表，实际上就一个
     */
    public List<PersonInfo> getCurrentUsers() {
        return personInofRepository.findAllByUser(userTools.getCurrentEruptUser().getAccount());
    }

    /**
     * 获取该用户的所在的项目（如果有多个就拿第一个）
     *
     * @return 用户的基础信息
     */
    public PersonInfo getCurrentUser() {
        List<PersonInfo> personInfos = personInofRepository.findAllByUser(userTools.getCurrentEruptUser().getAccount());
        if (personInfos.isEmpty()) throw new EruptApiErrorTip("你不在项目中，请确认是否做好关联！");
        return personInfos.get(0);
    }


}
