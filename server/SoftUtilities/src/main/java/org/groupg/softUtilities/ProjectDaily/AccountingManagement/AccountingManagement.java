package org.groupg.softUtilities.ProjectDaily.AccountingManagement;

import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.NumberType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.annotation.sub_field.sub_edit.VL;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 项目、个人账务管理
 * 项目经理可以申请项目的费用，但其他人只能申请自己的费用
 */
@Erupt(name = "账务管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_accountingmanagement_ins")    //数据库表名
@Entity
@Getter
@Setter
public class AccountingManagement extends HyperModelVo {


    /**
     * 记账类型
     */
    @EruptField(
            views = @View(title = "记账类型"),
            edit = @Edit(title = "记账类型", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "项目账务")
                            , @VL(value = "0", label = "个人账务")
                    })
                    , search = @Search))
    private String bookkeepingType = "0";

    /**
     * 账务类型
     */
    @EruptField(
            views = @View(title = "账务类型"),
            edit = @Edit(title = "账务类型", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "0", label = "支出")
                            , @VL(value = "1", label = "收入")
                    })
                    , search = @Search))
    private String accountingType = "0";

    /**
     * 账务事由
     */
    @EruptField(
            views = @View(title = "账务事由"),
            edit = @Edit(title = "账务事由"
                    , search = @Search))
    private String accountingMatter = "";

    /**
     * 记账金额
     */
    @EruptField(
    edit = @Edit(title = "记账金额", numberType = @NumberType)
    )
    private Double amount;

    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
        edit = @Edit(title = "分割线", type = EditType.DIVIDE)
    )
    private String divide;

    /**
     * 备注
     */
    @EruptField(
            views = @View(title = "备注"),
            edit = @Edit(title = "备注"
                    , search = @Search))
    private String remark = "";



}
