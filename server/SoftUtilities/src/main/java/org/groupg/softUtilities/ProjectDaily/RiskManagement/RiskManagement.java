package org.groupg.softUtilities.ProjectDaily.RiskManagement;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import org.groupg.softUtilities.ProjectDaily.TaskInfo.TaskInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 项目风险管理，风险包含风险名称、风险ID、风险负责人、风险类型、风险说明、解决方案
 */
@Erupt(name = "风险管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
//erupt类注解
@Table(name = "dtb_commons_risk_ins")    //数据库表名
@Entity
@Getter
@Setter
public class RiskManagement extends HyperModelVo {

    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
            edit = @Edit(title = "风险基本信息", type = EditType.DIVIDE)
    )
    private String divide1;

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_project_risk_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "risk_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "project_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "项目信息管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ProjectInfo> projectInfos;

    /**
     * 风险名称
     */
    @EruptField(
            views = @View(title = "风险名称"),
            edit = @Edit(title = "风险名称", search = @Search)
    )
    private String riskName = "";

    /**
     * 风险ID
     */
    @EruptField(
            views = @View(title = "风险ID"),
            edit = @Edit(title = "风险ID", search = @Search)
    )
    private String riskID = "";

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_person_risk_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "risk_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "person_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "风险管理人员",desc = "项目成员管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<PersonInfo> personInfos;

    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "任务id", column = "id"),
                    @View(title = "任务名称", column = "taskName")
            },
            edit = @Edit(title = "风险所属任务", desc = "该任务启动时会直接触发这个风险（若风险状态不是等待风险发生、风险应对策略不是不管，那就直接触发预警，会上报的预警）", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType(label = "taskName")
            )
    )
    private TaskInfo taskInfo;

    @EruptField(
            views = @View(title = "风险预计发生时间"),
            edit = @Edit(title = "风险预计发生时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date startDay;

    @EruptField(
            views = @View(title = "风险预计结束时间"),
            edit = @Edit(title = "风险预计结束时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date endDay;
    /**
     * 风险类型
     */
    @EruptField(
            views = @View(title = "风险类型"),
            edit = @Edit(title = "风险类型", desc = "注意，对于风险类型，在分析风险的时候可能会出现调整，这部分调整是允许且乐观的，风险只有分析了才有机会转化为问题或任务进行处理，否则是无法解决风险的。", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "进度风险")
                            , @VL(value = "2", label = "业务风险")
                            , @VL(value = "3", label = "需求风险")
                            , @VL(value = "4", label = "设计风险")
                            , @VL(value = "5", label = "开发风险")
                            , @VL(value = "6", label = "测试风险")
                            , @VL(value = "7", label = "投产风险")
                            , @VL(value = "8", label = "安全风险")
                            , @VL(value = "9", label = "验收风险")
                    })
                    , search = @Search))
    private String riskType = "1";


    /**
     * 风险状态
     */
    @EruptField(
            views = @View(title = "风险状态"),
            edit = @Edit(title = "风险状态", desc = "注意，风险状态，是项目组成员意识到风险之后，将风险的运作状态分为几个阶段", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "风险新增")
                            , @VL(value = "2", label = "分析风险原因")
                            , @VL(value = "3", label = "设计解决步骤")
                            , @VL(value = "4", label = "等待风险发生")
                            , @VL(value = "5", label = "风险发生")
                    })
                    , search = @Search))
    private String riskStatue = "1";
    /**
     * 风险简要说明
     */
    @EruptField(
            views = @View(title = "风险简要说明"),
            edit = @Edit(title = "风险简要说明", type = EditType.TEXTAREA, search = @Search(vague = true))
    )
    private String riskRemark = "";

    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
            edit = @Edit(title = "风险分析信息", desc = "可以让负责人分析后再填", type = EditType.DIVIDE)
    )
    private String divide2;
    /**
     * 风险应对策略
     */
    @EruptField(
            views = @View(title = "风险应对策略"),
            edit = @Edit(title = "风险应对策略", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "规避风险-换条路子，不做这个")
                            , @VL(value = "2", label = "接受风险-不管")
                            , @VL(value = "3", label = "降低风险-内部制定方案解决")
                            , @VL(value = "4", label = "分担风险-买保险，找领导")
                    })
                    , search = @Search))
    private String riskResponseStrategies;

    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "风险解决方案", type = ViewType.HTML, export = false),
            edit = @Edit(title = "风险解决方案",
                    type = EditType.HTML_EDITOR,
                    htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String riskContent;
}
