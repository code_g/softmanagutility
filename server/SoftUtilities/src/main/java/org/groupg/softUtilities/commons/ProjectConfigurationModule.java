package org.groupg.softUtilities.commons;

import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import org.groupg.softUtilities.ProjectConfiguration.ResourceInfo.ResourceInfo;
import org.groupg.softUtilities.ProjectConfiguration.SoftProject.SoftProject;
import org.groupg.softUtilities.ProjectConfiguration.WorkingCalendar.WorkingCalendar;
import org.groupg.softUtilities.commons.service.Default.DefaultEruptPostInfo;
import org.springframework.stereotype.Component;
import xyz.erupt.core.module.EruptModule;
import xyz.erupt.core.module.EruptModuleInvoke;
import xyz.erupt.core.module.MetaMenu;
import xyz.erupt.core.module.ModuleInfo;
import xyz.erupt.core.util.MD5Util;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.upms.model.EruptPost;
import xyz.erupt.upms.model.EruptUser;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class ProjectConfigurationModule implements EruptModule {

    @Resource
    private EruptDao eruptDao;

    static {
        EruptModuleInvoke.addEruptModule(ProjectConfigurationModule.class);
    }

    // 模块信息
    @Override
    public ModuleInfo info() {
        return ModuleInfo.builder().name("SUNDATA-ProjectConfiguration").build();
    }

    //初始化方法，每次启动时执行
    @Override
    public void run() {

    }

    // 初始化菜单 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public List<MetaMenu> initMenus() {
        List<MetaMenu> menus = new ArrayList<>();
        menus.add(MetaMenu.createRootMenu("$ProjectConfiguration", "项目基础信息管理", "fa fa-cogs", 100));
        menus.add(MetaMenu.createEruptClassMenu(ProjectInfo.class, menus.get(0), 10));
        menus.add(MetaMenu.createEruptClassMenu(PersonInfo.class, menus.get(0), 20));
        menus.add(MetaMenu.createEruptClassMenu(WorkingCalendar.class, menus.get(0), 30));
        menus.add(MetaMenu.createEruptClassMenu(ResourceInfo.class, menus.get(0), 40));
        menus.add(MetaMenu.createEruptClassMenu(SoftProject.class, menus.get(0), 50));
        return menus;
    }

    // 初始化方法 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public void initFun() {
        // 默认的成员角色需要配置齐全
        EruptUser eruptUser = new EruptUser();
        eruptUser.setIsAdmin(true);
        eruptUser.setIsMd5(true);
        eruptUser.setStatus(true);
        eruptUser.setCreateTime(new Date());
        eruptUser.setAccount("DEMO_P01_001");
        eruptUser.setPassword(MD5Util.digest("abc123"));
        eruptUser.setName("项目经理");
        eruptDao.persistIfNotExist(EruptUser.class, eruptUser, "account", eruptUser.getAccount());
        // 初始化例子项目
        // 包括项目相关的一切 ，但不同部分的配置只适用于另外的模块，本模块只适用于项目基础配置以及基础用户、角色配置

        ;
        for ( EruptPost post : DefaultEruptPostInfo.getDefaultPostList()) {
            eruptDao.persistIfNotExist(EruptPost.class, post, "code", post.getCode());
        }
    }
}
