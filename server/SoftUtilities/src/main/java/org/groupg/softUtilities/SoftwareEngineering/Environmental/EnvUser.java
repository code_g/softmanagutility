package org.groupg.softUtilities.SoftwareEngineering.Environmental;

import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.InputType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 硬件环境的用户
 */
@Erupt(name = "运行环境用户管理"
        , power = @Power(importable = true, export = true))        //erupt类注解
@Table(name = "dtb_commons_environmental_user_ins")    //数据库表名
@Entity
@Getter
@Setter
public class EnvUser extends HyperModelVo {

    @EruptField(
            views = @View(title = "用户说明"),
            edit = @Edit(title = "用户说明", search = @Search)
    )
    private String name;

    @EruptField(
            views = @View(title = "登录用户名"),
            edit = @Edit(title = "登录用户名", search = @Search)
    )
    private String account;

    @EruptField(
            views = @View(title = "登录用户密码"),
            edit = @Edit(title = "登录用户密码", inputType = @InputType(type = "password"), search = @Search)
    )
    private String password;
}
