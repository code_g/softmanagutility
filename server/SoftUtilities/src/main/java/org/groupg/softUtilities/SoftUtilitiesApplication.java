package org.groupg.softUtilities;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import xyz.erupt.core.annotation.EruptScan;

@SpringBootApplication
@EntityScan
@EruptScan
public class SoftUtilitiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoftUtilitiesApplication.class, args);
    }

}
