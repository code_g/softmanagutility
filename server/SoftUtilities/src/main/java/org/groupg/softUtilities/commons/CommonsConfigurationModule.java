package org.groupg.softUtilities.commons;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.erupt.core.module.EruptModule;
import xyz.erupt.core.module.EruptModuleInvoke;
import xyz.erupt.core.module.MetaMenu;
import xyz.erupt.core.module.ModuleInfo;
import xyz.erupt.upms.service.EruptUserService;

import javax.annotation.Resource;
import java.util.List;


@Component
public class CommonsConfigurationModule implements EruptModule {

    static {
        EruptModuleInvoke.addEruptModule(CommonsConfigurationModule.class);
    }

    @Resource
    EruptUserService userService;

    // 模块信息
    @Override
    public ModuleInfo info() {
        return ModuleInfo.builder().name("SUNDATA-CommonsConfigurationModule").build();
    }

    //初始化方法，每次启动时执行
    @Override
    public void run() {

    }

    // 初始化菜单 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public List<MetaMenu> initMenus() {
        return null;
    }

    // 初始化方法 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public void initFun() {

    }

}
