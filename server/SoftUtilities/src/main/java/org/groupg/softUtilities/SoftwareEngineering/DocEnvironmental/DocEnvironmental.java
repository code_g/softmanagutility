package org.groupg.softUtilities.SoftwareEngineering.DocEnvironmental;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.SoftwareEngineering.Environmental.CriticalPath;
import org.groupg.softUtilities.SoftwareEngineering.Environmental.EnvUser;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Set;


@Erupt(name = "文档版本环境管理"
        , power = @Power(importable = true, export = true))        //erupt类注解
@Table(name = "dtb_commons_docenvironmental_ins")    //数据库表名
@Entity
@Getter
@Setter
public class DocEnvironmental extends HyperModelVo {
    @EruptField(
            views = @View(title = "文档库名称"),
            edit = @Edit(title = "文档库名称", search = @Search)
    )
    private String name = "默认代码库";

    // 版本管理工具：
    // 项目首页
    // 项目版本管理链接（git的 ssh 之类的）

    @EruptField(
            views = @View(title = "文档库URL链接",desc = "用于查看文档库的链接",type = ViewType.LINK),
            edit = @Edit(title = "文档库URL链接",desc = "用于查看文档库的链接", search = @Search)
    )
    private String docUrl = "";

    @EruptField(
            views = @View(title = "文档库管理链接",desc = "用来做 clone 或 check out 的链接",type = ViewType.LINK),
            edit = @Edit(title = "文档库管理链接",desc = "用来做 clone 或 check out 的链接", search = @Search(vague = true))
    )
    private String docPath = "";

    @Lob
    @EruptField(
            views = @View(title = "文档库描述"),
            edit = @Edit(title = "文档库描述", type = EditType.TEXTAREA)
    )
    private String remark;

    @ManyToMany //多对多
    @JoinTable(name = "docenvironmental_user", //定义多对多中间表
            joinColumns = @JoinColumn(name = "env_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "env_user_id", referencedColumnName = "id"))
    @EruptField(
            edit = @Edit(
                    title = "文档库用户",desc = "运行环境用户管理中的数据",
                    type = EditType.TAB_TABLE_REFER
            )
    )
    private Set<EnvUser> envUsers; //Table对象定义如下👇

    @ManyToMany //多对多
    @JoinTable(name = "docenvironmental_criticalpath", //定义多对多中间表
            joinColumns = @JoinColumn(name = "env_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "env_path_id", referencedColumnName = "id"))
    @EruptField(
            edit = @Edit(
                    title = "关键文档",desc = "关键路径管理中的数据",
                    type = EditType.TAB_TABLE_REFER
            )
    )
    private Set<CriticalPath> criticalPaths;
}
