package org.groupg.softUtilities.ProjectDaily.LoanBillManagement;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import org.groupg.softUtilities.commons.utils.MoneyUtils;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.*;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;
import xyz.erupt.upms.model.EruptUser;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * 借款单
 */
@Erupt(name = "借款单管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_loanBillmanagement_ins")    //数据库表名
@Entity
@Getter
@Setter
public class LoanBillManagement extends HyperModelVo {

    /**
     * 所属项目
     */
    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "项目ID", column = "id"),
                    @View(title = "项目名称", column = "name")
            },
            edit = @Edit(title = "所属项目", desc = "需要先完成项目的配置才能使用这个", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType()
            )
    )
    private ProjectInfo project;

    /**
     * 借款人
     */
    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "用户名", column = "account"),
                    @View(title = "姓名", column = "name")
            },
            edit = @Edit(title = "借款人", desc = "注意：需要先在用户中心完成用户的新增之后才能在这里使用用户", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType()
            )
    )
    private EruptUser user;

    /**
     * 借款金额
     */
    @EruptField(
        edit = @Edit(title = "借款金额", numberType = @NumberType)
    )
    private Double loanAmount;

    public void setLoanAmount(Double loanAmount) {
        this.capitalization = MoneyUtils.convert(loanAmount);
        this.loanAmount = loanAmount;
    }

    /**
     * 借款日期
     */
    @EruptField(
        views = @View(title = "借款日期"),
        edit = @Edit(title = "借款日期",
            dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
            , search = @Search))
    private Date startDate;
    /**
     * 使用日期
     */
    @EruptField(
        views = @View(title = "使用日期"),
        edit = @Edit(title = "使用日期",
            dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
            , search = @Search))
    private Date useDate;

    /**
     * 到账日期
     */
    @EruptField(
        views = @View(title = "到账日期"),
        edit = @Edit(title = "到账日期", type = EditType.HIDDEN
                ,dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL)
                , search = @Search) //选择历史时间
    )
    private Date obtainDate;

    /**
     * 借款状态
     */
    @EruptField(
            views = @View(title = "借款状态"),
            edit = @Edit(title = "借款状态", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "0", label = "未申请")
                            , @VL(value = "1", label = "写了借款条，但还未邮寄")
                            , @VL(value = "2", label = "已邮寄")
                            , @VL(value = "3", label = "已放款")
                    })
                    , search = @Search))
    private String borrowingStatus = "0";

    public void setBorrowingStatus(String borrowingStatus){
        if (this.getObtainDate() == null && StringUtils.equals(borrowingStatus,"3")){
            this.setObtainDate(new Date());
        }
        this.borrowingStatus = borrowingStatus;
    }

;
    /**
     * 借款金额大写
     */
    @Lob
    @EruptField(
    views = @View(title = "借款金额大写"),
    edit = @Edit(title = "借款金额大写", type = EditType.TEXTAREA,readonly = @Readonly(add = false,edit = false),search = @Search(vague = true))
)
    private String capitalization;

    /**
     * 借款用途
     */
    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "借款用途", type = ViewType.HTML),
            edit = @Edit(title = "借款用途",
                     type = EditType.HTML_EDITOR,
                     htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String remark;
}
