package org.groupg.softUtilities.ProjectConfiguration.WorkingCalendar;

import cn.hutool.extra.spring.SpringUtil;
import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.utils.PersonInfoUtils;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.DateType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.annotation.sub_field.sub_edit.VL;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Erupt(name = "工作日历"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
//erupt类注解
@Table(name = "dtb_commons_workingcalendar_ins")    //数据库表名
@Entity
@Getter
@Setter
public class WorkingCalendar extends HyperModelVo {


    /**
     * 日历名称
     */
    @EruptField(
            views = @View(title = "日历名称"),
            edit = @Edit(title = "日历名称", search = @Search)
    )
    private String name;


    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_project_workingcalendar_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "workingcalendar_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "project_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "项目信息管理的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ProjectInfo> projectInfos;
    /**
     * 日历名称
     */
    @EruptField(
            views = @View(title = "日历类型"),
            edit = @Edit(title = "日历类型", search = @Search, type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "0", label = "工作日日历")
                            , @VL(value = "1", label = "假期日历")
                            , @VL(value = "2", label = "休息日工作日历")
                            , @VL(value = "3", label = "特殊日历")
                    })
            )
    )
    private String calendarType;


    @EruptField(
            views = @View(title = "开始日期"),
            edit = @Edit(title = "开始日期",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date startDay;

    @EruptField(
            views = @View(title = "结束日期"),
            edit = @Edit(title = "结束日期",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date endDay;

    @EruptField(
            views = @View(title = "上午上班时间"),
            edit = @Edit(title = "上午上班时间", type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private String dAMStartDay;

    @EruptField(
            views = @View(title = "上午下班时间"),
            edit = @Edit(title = "上午下班时间", type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private String dAMEndDay;

    @EruptField(
            views = @View(title = "下午上班时间"),
            edit = @Edit(title = "下午上班时间", type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private String dPMStartDay;

    @EruptField(
            views = @View(title = "下午下班时间"),
            edit = @Edit(title = "下午下班时间", type = EditType.DATE,
                    dateType = @DateType(type = DateType.Type.TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private String dPMEndDay;

    @EruptField(
            views = @View(title = "备注"),
            edit = @Edit(title = "备注", type = EditType.TEXTAREA, search = @Search(vague = true))
    )
    private String remark;

    public static WorkingCalendar createDefault() {
        WorkingCalendar workingCalendar = new WorkingCalendar();
        workingCalendar.projectInfos = SpringUtil.getBean("PersonInfoUtils", PersonInfoUtils.class).getCurrentUser().getProjectInfos();
        workingCalendar.calendarType = "1";
        return workingCalendar;
    }

}
