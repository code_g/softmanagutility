package org.groupg.softUtilities.commons;

import org.groupg.softUtilities.SoftwareEngineering.CodeEnvironmental.CodeEnvironmental;
import org.groupg.softUtilities.SoftwareEngineering.DocEnvironmental.DocEnvironmental;
import org.groupg.softUtilities.SoftwareEngineering.Environmental.CriticalPath;
import org.groupg.softUtilities.SoftwareEngineering.Environmental.EnvUser;
import org.groupg.softUtilities.SoftwareEngineering.Environmental.Environmental;
import org.groupg.softUtilities.SoftwareEngineering.Requirements.Requirements;
import org.groupg.softUtilities.SoftwareEngineering.Versioning.Versioning;
import org.springframework.stereotype.Component;
import xyz.erupt.core.module.EruptModule;
import xyz.erupt.core.module.EruptModuleInvoke;
import xyz.erupt.core.module.MetaMenu;
import xyz.erupt.core.module.ModuleInfo;

import java.util.ArrayList;
import java.util.List;


@Component
public class SoftwareEngineeringModule implements EruptModule {


    static {
        EruptModuleInvoke.addEruptModule(SoftwareEngineeringModule.class);
    }

    // 模块信息
    @Override
    public ModuleInfo info() {
        return ModuleInfo.builder().name("SUNDATA-SoftwareEngineering").build();
    }

    //初始化方法，每次启动时执行
    @Override
    public void run() {

    }

    // 初始化菜单 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public List<MetaMenu> initMenus() {
        List<MetaMenu> menus = new ArrayList<>();
        menus.add(MetaMenu.createRootMenu("$SoftwareEngineering", "软件工程管理", "fa fa-cogs", 300));
        menus.add(MetaMenu.createEruptClassMenu(Environmental.class, menus.get(0), 10));
        menus.add(MetaMenu.createEruptClassMenu(EnvUser.class, menus.get(0), 20));
        menus.add(MetaMenu.createEruptClassMenu(CriticalPath.class, menus.get(0), 30));
        menus.add(MetaMenu.createEruptClassMenu(CodeEnvironmental.class, menus.get(0), 40));
        menus.add(MetaMenu.createEruptClassMenu(DocEnvironmental.class, menus.get(0), 50));
        menus.add(MetaMenu.createEruptClassMenu(Requirements.class, menus.get(0), 60));
        menus.add(MetaMenu.createEruptClassMenu(Versioning.class, menus.get(0), 60));


        return menus;
    }

    // 初始化方法 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public void initFun() {

    }
}
