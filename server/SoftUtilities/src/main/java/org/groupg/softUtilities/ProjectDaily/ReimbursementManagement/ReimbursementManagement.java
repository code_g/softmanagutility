package org.groupg.softUtilities.ProjectDaily.ReimbursementManagement;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import org.groupg.softUtilities.commons.utils.MoneyUtils;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.Readonly;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;
import xyz.erupt.upms.model.EruptUser;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 报销单
 */
@Erupt(name = "报销单管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_reimbursementmanagement_ins")    //数据库表名
@Entity
@Getter
@Setter
public class ReimbursementManagement extends HyperModelVo {

    /**
     * 所属项目
     */
    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "项目ID", column = "id"),
                    @View(title = "项目名称", column = "name")
            },
            edit = @Edit(title = "所属项目", desc = "需要先完成项目的配置才能使用这个", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType()
            )
    )
    private ProjectInfo project;

    /**
     * 借款人
     */
    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "用户名", column = "account"),
                    @View(title = "姓名", column = "name")
            },
            edit = @Edit(title = "报销人", desc = "注意：需要先在用户中心完成用户的新增之后才能在这里使用用户，报销人与出差人是一个人", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType()
            )
    )
    private EruptUser user;

    /**
     * 报销事由
     */
    @EruptField(
            views = @View(title = "报销事由"),
            edit = @Edit(title = "报销事由",
                     type = EditType.TEXTAREA)
    )
    private String remark;

    /**
     * 报销单状态
     */
    @EruptField(
            views = @View(title = "报销状态"),
            edit = @Edit(title = "报销状态", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "0", label = "未申请")
                            , @VL(value = "1", label = "写了报销单，但还未邮寄")
                            , @VL(value = "2", label = "已邮寄")
                            , @VL(value = "3", label = "已审批")
                            , @VL(value = "4", label = "已放款")
                    })
                    , search = @Search))
    private String reimbursementStatus;

    public void setReimbursementStatus(String reimbursementStatus){
        if (this.getObtainDate() == null && StringUtils.equals(reimbursementStatus,"3")){
            this.setObtainDate(new Date());
        }
        this.reimbursementStatus = reimbursementStatus;
    }
    /**
     * 到账日期
     */
    @EruptField(
        views = @View(title = "到账日期"),
        edit = @Edit(title = "到账日期", type = EditType.HIDDEN
                ,dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL)
                , search = @Search) //选择历史时间
    )
    private Date obtainDate;

    /**
     * 报销总额
     */
    @EruptField(
        edit = @Edit(title = "报销总额", numberType = @NumberType)
    )
    private Double expenseMount = 0.0;

    public void setExpenseMount(Double expenseMount) {
        if (expenseMount != null)
            this.capitalization = MoneyUtils.convert(expenseMount);
        this.expenseMount = expenseMount;
    }

    /**
     * 报销总额大写
     */
    @Lob
    @EruptField(
    views = @View(title = "报销总额大写"),
    edit = @Edit(title = "报销总额大写", type = EditType.TEXTAREA,readonly = @Readonly(add = false,edit = false),search = @Search(vague = true))
)
    private String capitalization;

//    /**
//     * 报销单类型 暂不设置
//     */
//    @EruptField(
//            views = @View(title = "报销单类型"),
//            edit = @Edit(title = "报销单类型", type = EditType.CHOICE,
//                    choiceType = @ChoiceType(vl = {
//                            @VL(value = "0", label = "费用报销单")
//                            , @VL(value = "1", label = "差旅报销单")
//                    })
//                    , search = @Search))
//    private String reiType = "0";

    /**
     * 差旅报销项目 Set
     */
    // 注：orphanRemoval 配置在 1.6.4 版本以后开始支持
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true) //一对多，且开启级联
    @JoinColumn(name = "reimbursement_id") //this表示当前的表名，如：order_id子表会自动创建该列来标识与主表的关系
    @OrderBy //排序
    @EruptField(
        edit = @Edit(title = "差旅报销项目", type = EditType.TAB_TABLE_ADD)
    )
    private Set<ReimbursementItemTravels> travels;

    /**
     * 费用报销项目 Set
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true) //一对多，且开启级联
    @JoinColumn(name = "reimbursement_id") //this表示当前的表名，如：order_id子表会自动创建该列来标识与主表的关系
    @OrderBy //排序
    @EruptField(
        edit = @Edit(title = "费用报销项目", type = EditType.TAB_TABLE_ADD)
    )
    private Set<ReimbursementItemCosts> costs;

}
