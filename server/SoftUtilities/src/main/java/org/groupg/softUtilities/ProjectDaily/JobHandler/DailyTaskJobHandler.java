package org.groupg.softUtilities.ProjectDaily.JobHandler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.erupt.core.annotation.EruptHandlerNaming;
import xyz.erupt.job.handler.EruptJobHandler;


@Service
@EruptHandlerNaming("每日处理日报内容")  // 如果不添加此配置，类名会作为前端展示依据
@Slf4j
/**
 * 每日处理日报内容，结算每天的任务
 * @author ligengpu
 */
public class DailyTaskJobHandler implements EruptJobHandler {
    /**
     * @param code
     * @param param
     * @return
     */
    @Override
    public String exec(String code, String param) {
        log.debug("'DailyTaskJobHandler' -------- code:{},param:{}", code, param);
        if (param.contains("a")) {
            try {
                throw new Exception("demo异常");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return "完成";
    }

    /**
     * 如果正常结束，会执行这部分功能作为后置程序
     *
     * @param result
     * @param param
     */
    @Override
    public void success(String result, String param) {
        log.debug("'DailyTaskJobHandler' -------- result:{},param:{}", result, param);
        EruptJobHandler.super.success(result, param);
    }

    /**
     * 如果程序出现异常 则会使用该异常调用该方法。
     *
     * @param throwable
     * @param param
     * @link org.groupg.SoftUtilities.ProjectDaily.JobHandler.DailyTaskJobHandler#exec()
     */
    @Override
    public void error(Throwable throwable, String param) {
        log.debug("'DailyTaskJobHandler' -------- throwable:{},param:{}", throwable.getMessage(), param);
        EruptJobHandler.super.error(throwable, param);
    }
}
