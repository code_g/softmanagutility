package org.groupg.softUtilities.SoftwareEngineering.Versioning;


import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import org.groupg.softUtilities.ProjectConfiguration.SoftProject.SoftProject;
import org.groupg.softUtilities.ProjectDaily.ModifyRegistration.ModifyRegistration;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;


@Erupt(name = "版本管理"
        , power = @Power(importable = true, export = true))        //erupt类注解
@Table(name = "dtb_commons_versioning_ins")    //数据库表名
@Entity
@Getter
@Setter
public class Versioning  extends HyperModelVo {

    @EruptField(
            views = @View(title = "版本名称"),
            edit = @Edit(title = "版本名称", search = @Search)
    )
    private String name;

    @ManyToOne //多对一
    @JoinTable(name = "dtb_commons_softproject_versioning_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "versioning_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "softproject_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "软件项目信息管理中的数据",
            type = EditType.REFERENCE_TREE
        )
    )
    private SoftProject softProjects;

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_modifyregistration_versioning_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "modify_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "versioning_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "选择修改内容",desc = "修改登记功能中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ModifyRegistration> modifyRegistrations;

    @ManyToOne //多对多
    @JoinTable(name = "dtb_commons_person_versioning_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "verson_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "person_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "投产负责人",desc = "项目成员管理中的数据",
            type = EditType.REFERENCE_TABLE
        )
    )
    private PersonInfo personInfo;

    @EruptField(
        views = @View(title = "投产状态"),
        edit = @Edit(title = "投产状态", type = EditType.CHOICE,
                choiceType = @ChoiceType(vl = {
                        @VL(value = "1", label = "待投产")
                        , @VL(value = "2", label = "已完成")
                })
                , search = @Search))
    private String versionStatue = "1";

    @EruptField(
        views = @View(title = "投产时间"),
        edit = @Edit(title = "投产时间",
                dateType = @DateType(pickerMode = DateType.PickerMode.ALL, type = DateType.Type.DATE_TIME) //选择历史时间
                , search = @Search))
    private Date datetime;


    /**
     * 投产包链接，指定的链接需要
     */
    @Transient
    @EruptField(
        views = @View(title = "投产包",type = ViewType.LINK)
//            ,edit = @Edit(title = "投产包",type = EditType.HIDDEN)
    )
    // TODO 需要处理该功能的请求，该功能请求需要使用导出文件以及相关处理，投产包不保留历史
    private String productionPackage= "";

    @Override
    public void setId(Long id) {
        this.productionPackage = "/getVersioningPackage/"+this.getId();
        super.setId(id);
    }

    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "投产说明", type = ViewType.HTML, export = false),
            edit = @Edit(title = "投产说明",
                     type = EditType.HTML_EDITOR,
                     htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String content;
}
