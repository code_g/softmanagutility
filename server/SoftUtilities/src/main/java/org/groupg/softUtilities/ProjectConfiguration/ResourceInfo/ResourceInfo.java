package org.groupg.softUtilities.ProjectConfiguration.ResourceInfo;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.DateType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.annotation.sub_field.sub_edit.VL;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * TODO 项目资源信息，暂不处理
 */
@Erupt(name = "项目资源管理"
        , power = @Power(importable = true, export = true)
        , desc = "针对资源的花销登记，类似于一个项目的记账"
        /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)         //erupt类注解
@Table(name = "dtb_commons_resourceinfo_ins")    //数据库表名
@Entity
@Getter
@Setter
public class ResourceInfo extends HyperModelVo {


    @EruptField(
            views = @View(title = "资源名称"),
            edit = @Edit(title = "资源名称", search = @Search(vague = true))
    )
    public String name;

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_project_resource_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "resource_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "project_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "项目信息管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ProjectInfo> projectInfos;

    @EruptField(
            views = @View(title = "开始日期"),
            edit = @Edit(title = "开始日期",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date startDay;

    @EruptField(
            views = @View(title = "结束日期"),
            edit = @Edit(title = "结束日期",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date endDay;

    @EruptField(
            views = @View(title = "费用次数"),
            edit = @Edit(title = "费用次数", type = EditType.NUMBER
                    , search = @Search))
    private Integer frequencyNum;
    @EruptField(
            views = @View(title = "费用频率"),
            edit = @Edit(title = "费用频率", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "一次性费用")
                            , @VL(value = "2", label = "天")
                            , @VL(value = "3", label = "工作日") // 工作日处理的需要结合日历，如果该项目没有日历，则默认为当前工作日
                            , @VL(value = "4", label = "周")
                            , @VL(value = "5", label = "月")
                            , @VL(value = "6", label = "季")
                            , @VL(value = "7", label = "年")
                    })
                    , search = @Search))
    private Integer frequency;


    @EruptField(
            views = @View(title = "单次费用"),
            edit = @Edit(title = "单次费用", type = EditType.NUMBER
                    , search = @Search(vague = true)))
    private Double cost;


}
