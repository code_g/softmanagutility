package org.groupg.softUtilities.ProjectDaily.TaskInfo;

import cn.hutool.extra.spring.SpringUtil;
import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.utils.PersonInfoUtils;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.utils.ProjectUtils;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Erupt(name = "工作任务"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
//erupt类注解
@Table(name = "dtb_commons_taskinfo_ins")    //数据库表名
@Entity
@Getter
@Setter
public class TaskInfo extends HyperModelVo {

    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
            edit = @Edit(title = "任务基本信息", type = EditType.DIVIDE)
    )
    private String divide1;

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_project_task_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "task_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "project_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "项目信息管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ProjectInfo> projectInfos;

    /**
     * 任务名称
     */
    @EruptField(
            views = @View(title = "任务名称"),
            edit = @Edit(title = "任务名称", search = @Search)
    )
    @Column(name = "taskName")
    private String taskName = "";

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_person_task_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "task_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "person_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "任务参与人员",desc = "项目成员管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<PersonInfo> personInfos;

    /**
     * 任务说明
     */
    @EruptField(
            views = @View(title = "任务说明"),
            edit = @Edit(title = "任务说明", type = EditType.TEXTAREA, search = @Search(vague = true))
    )
    @Column(name = "taskRemark")
    private String taskRemark = "";


    @Transient //由于该字段不需要持久化，所以使用该注解修饰
    @EruptField(
            edit = @Edit(title = "任务配置信息", type = EditType.DIVIDE)
    )
    private String divide2;

    /**
     * 任务阶段
     */
    @EruptField(
            views = @View(title = "任务阶段"),
            edit = @Edit(title = "任务阶段", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "商务活动")
                            , @VL(value = "2", label = "合同阶段")
                            , @VL(value = "3", label = "入场阶段")
                            , @VL(value = "4", label = "需求阶段")
                            , @VL(value = "5", label = "设计阶段")
                            , @VL(value = "6", label = "开发阶段")
                            , @VL(value = "7", label = "测试阶段")
                            , @VL(value = "8", label = "验收阶段")
                            , @VL(value = "9", label = "投产阶段")
                            , @VL(value = "10", label = "培训阶段")
                            , @VL(value = "11", label = "试运行阶段")
                            , @VL(value = "12", label = "运维阶段")
                    })
                    , search = @Search))
    @Column(name = "taskStage")
    private String taskStage = "1";

    /**
     * 任务类型
     */
    @EruptField(
            views = @View(title = "任务类型"),
            edit = @Edit(title = "任务类型", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "普通任务")
                            , @VL(value = "2", label = "问题")
                    })
                    , search = @Search))
    @Column(name = "taskType")
    private String taskType = "1";

    /**
     * 任务状态
     */
    @EruptField(
            views = @View(title = "任务状态"),
            edit = @Edit(title = "任务状态", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "未开始")
                            , @VL(value = "2", label = "正在进行")
                            , @VL(value = "3", label = "已完成")
                    })
                    , search = @Search))
    @Column(name = "taskStatue")
    private String taskStatue = "1";
    /**
     * 任务进度
     */
    @EruptField(
            views = @View(title = "任务说明(%)"),
            edit = @Edit(title = "任务进度(%)", type = EditType.SLIDER, sliderType = @SliderType(max = 100))
    )
    @Column(name = "schedule")
    private Integer schedule = 0;

    /**
     * 紧急程度
     */
    @EruptField(
            views = @View(title = "紧急程度"),
            edit = @Edit(title = "紧急程度", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "不紧急")
                            , @VL(value = "2", label = "紧急")
                            , @VL(value = "3", label = "非常紧急")
                    })
                    , search = @Search))
    @Column(name = "urgency")
    private String urgency = "1";

    /**
     * 严重程度
     */
    @EruptField(
            views = @View(title = "严重程度"),
            edit = @Edit(title = "严重程度", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "不严重")
                            , @VL(value = "2", label = "严重")
                            , @VL(value = "3", label = "非常严重")
                    })
                    , search = @Search))
    @Column(name = "severity")
    private String severity = "1";


    @EruptField(
            views = @View(title = "计划开始时间"),
            edit = @Edit(title = "计划开始时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "startDay")
    private Date startDay;

    @EruptField(
            views = @View(title = "计划结束时间"),
            edit = @Edit(title = "计划结束时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "endDay")
    private Date endDay;

    @EruptField(
            views = @View(title = "计划工作量"),
            edit = @Edit(title = "计划工作量(人天)", type = EditType.NUMBER)
    )
    private Double workload;

    @EruptField(
            views = @View(title = "实际开始时间"),
            edit = @Edit(title = "实际开始时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "startDayOver")
    private Date startDayOver;

    @EruptField(
            views = @View(title = "实际结束时间"),
            edit = @Edit(title = "实际结束时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "endDayOver")
    private Date endDayOver;

    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "详细任务说明", type = ViewType.HTML, export = false),
            edit = @Edit(title = "详细任务说明",
                     type = EditType.HTML_EDITOR,
                     htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String content;

    /**
     * 创建默认对象使用的方法，可以直接指定当前登录人的方式
     *
     * @return
     */
    public static TaskInfo createDefault() {
        TaskInfo taskInfo = new TaskInfo();
        Set<ProjectInfo> projectInfos1 = new HashSet<>();
        projectInfos1.add(SpringUtil.getBean("ProjectUtils", ProjectUtils.class).getCurrentUserProject());
        taskInfo.projectInfos = projectInfos1;
        Set<PersonInfo> personInfos1 = new HashSet<>();
        personInfos1.add(SpringUtil.getBean("PersonInfoUtils", PersonInfoUtils.class).getCurrentUser());
        taskInfo.personInfos = personInfos1;
        return taskInfo;
    }
}
