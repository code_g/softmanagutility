package org.groupg.softUtilities.ProjectDaily.TaskProgress;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectDaily.TaskInfo.TaskInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.NumberType;
import xyz.erupt.annotation.sub_field.sub_edit.ReferenceTableType;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Erupt(name = "任务进度"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
//erupt类注解
@Table(name = "dtb_commons_taskprogress_ins")    //数据库表名
@Entity
@Getter
@Setter
/**
 * 任务进度只能被更新，不能被删除，所以需要屏蔽更新方法，而且如果任务更新到了进度100 则认为完成，日批之后不能再更新。
 */
public class TaskProgress extends HyperModelVo {

    @ManyToOne //多对一
    @EruptField(
        views = {
            @View(title = "任务编号", column = "id"),
            @View(title = "任务名称", column = "taskName")
        },
        edit = @Edit(title = "更新任务", type = EditType.REFERENCE_TABLE,
            referenceTableType = @ReferenceTableType(label = "taskName")
        )
    )
    private TaskInfo taskInfo;

    /**
     * 当前进度
     */
    @EruptField(
            views = @View(title = "当前进度(%)"),
            edit = @Edit(title = "当前进度(%)", numberType = @NumberType(min = 0, max = 100))
    )
    private Integer schedule = 0;


    /**
     * 当前进度说明
     */
    @EruptField(
            views = @View(title = "当前进度说明"),
            edit = @Edit(title = "当前进度说明", type = EditType.TEXTAREA)
    )
    private String taskRemark = "";
}
