package org.groupg.softUtilities.ProjectConfiguration.ProjectInfo;


import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;
import xyz.erupt.upms.model.EruptUser;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Erupt(name = "项目信息管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
//erupt类注解
@Table(name = "dtb_commons_projectinfo_ins")    //数据库表名
@Entity
@Getter
@Setter
public class ProjectInfo extends HyperModelVo {

    @EruptField(
            views = @View(title = "项目名称"),
            edit = @Edit(title = "项目名称", search = @Search)
    )
    private String name;

    @EruptField(
            views = @View(title = "项目类型"),
            edit = @Edit(title = "项目类型", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "项目制"),
                            @VL(value = "2", label = "人力制"),
                            @VL(value = "3", label = "运维制")
                    })
                    , search = @Search))
    private Integer type;

    @EruptField(
            views = @View(title = "项目状态"),
            edit = @Edit(title = "项目状态", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "合同阶段"),
                            @VL(value = "2", label = "项目开发阶段")
                            , @VL(value = "3", label = "项目试运行阶段")
                            , @VL(value = "4", label = "项目验收阶段")
                            , @VL(value = "5", label = "运维阶段")
                    })
                    , search = @Search))
    private Integer status;

    @EruptField(
            views = @View(title = "开始日期"),
            edit = @Edit(title = "开始日期",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date startDay;

    @EruptField(
            views = @View(title = "结束日期"),
            edit = @Edit(title = "结束日期",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date endDay;

    @EruptField(
            views = @View(title = "简单说明"),
            edit = @Edit(title = "简单说明", type = EditType.TEXTAREA, search = @Search(vague = true))
    )
    private String remark;

    /*
     * 仅需将 fetchHandler 定义为 SqlChoiceFetchHandler 即可
     * fetchHandlerParams 参数值为 SQL 语句
     */
//    @EruptField(
//            views = @View(title = "项目经理"),
//            edit = @Edit(
//                    search = @Search,
//                    title = "项目经理",
//                    type = EditType.CHOICE,
//                    choiceType = @ChoiceType(
//                            fetchHandler = SqlChoiceFetchHandler.class,
//                            //参数一必填，表示sql语句
//                            //参数二可不填，表示缓存时间，默认为3000毫秒，1.6.10及以上版本支持
//                            fetchHandlerParams = {"select account as id, name from e_upms_user", "5000"}
//                    ))
//    )
//    private String projectManager;

    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "用户名", column = "account"),
                    @View(title = "姓名", column = "name")
            },
            edit = @Edit(title = "项目经理", desc = "注意：需要先在用户中心完成用户的新增之后才能在这里添加用户", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType()
            )
    )
    private EruptUser projectManager;
//    @OneToOne(cascade = CascadeType.ALL) // 不能用
//    @EruptField(
//        views = @View(title = "项目经理", column = "name"),
//        edit = @Edit(title = "项目经理", type = EditType.COMBINE)
//    )
//    private EruptUser manager;
}
