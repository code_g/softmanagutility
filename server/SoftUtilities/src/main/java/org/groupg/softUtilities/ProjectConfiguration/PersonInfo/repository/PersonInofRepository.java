package org.groupg.softUtilities.ProjectConfiguration.PersonInfo.repository;


import org.groupg.softUtilities.ProjectConfiguration.PersonInfo.PersonInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface PersonInofRepository extends JpaRepository<PersonInfo, Long> {
    List<PersonInfo> findAllByUser(String account);
}
