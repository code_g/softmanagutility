package org.groupg.softUtilities.commons;


import org.groupg.softUtilities.ProjectDaily.AccountingManagement.AccountingManagement;
import org.groupg.softUtilities.ProjectDaily.InstructionsAndMessages.InstructionsAndMessages;
import org.groupg.softUtilities.ProjectDaily.LeaveManagement.LeaveManagement;
import org.groupg.softUtilities.ProjectDaily.LoanBillManagement.LoanBillManagement;
import org.groupg.softUtilities.ProjectDaily.MeetingManagement.MeetingManagement;
import org.groupg.softUtilities.ProjectDaily.ModifyRegistration.ModifyRegistration;
import org.groupg.softUtilities.ProjectDaily.ReimbursementManagement.ReimbursementItemCosts;
import org.groupg.softUtilities.ProjectDaily.ReimbursementManagement.ReimbursementItemTravels;
import org.groupg.softUtilities.ProjectDaily.ReimbursementManagement.ReimbursementManagement;
import org.groupg.softUtilities.ProjectDaily.RiskManagement.RiskManagement;
import org.groupg.softUtilities.ProjectDaily.TaskInfo.TaskInfo;
import org.groupg.softUtilities.ProjectDaily.TaskProgress.TaskProgress;
import org.springframework.stereotype.Component;
import xyz.erupt.core.module.EruptModule;
import xyz.erupt.core.module.EruptModuleInvoke;
import xyz.erupt.core.module.MetaMenu;
import xyz.erupt.core.module.ModuleInfo;

import java.util.ArrayList;
import java.util.List;


@Component
public class ProjectDailyModule implements EruptModule {


    static {
        EruptModuleInvoke.addEruptModule(ProjectDailyModule.class);
    }

    // 模块信息
    @Override
    public ModuleInfo info() {
        return ModuleInfo.builder().name("SUNDATA-ProjectDaily").build();
    }

    //初始化方法，每次启动时执行
    @Override
    public void run() {

    }

    // 初始化菜单 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public List<MetaMenu> initMenus() {
        List<MetaMenu> menus = new ArrayList<>();
        menus.add(MetaMenu.createRootMenu("$ProjectDaily", "项目日常运行管理", "fa fa-cogs", 200));
        menus.add(MetaMenu.createEruptClassMenu(TaskInfo.class, menus.get(0), 10));
        menus.add(MetaMenu.createEruptClassMenu(TaskProgress.class, menus.get(0), 20));
        menus.add(MetaMenu.createEruptClassMenu(MeetingManagement.class, menus.get(0), 30));
        menus.add(MetaMenu.createEruptClassMenu(LeaveManagement.class, menus.get(0), 40));
        menus.add(MetaMenu.createEruptClassMenu(ModifyRegistration.class, menus.get(0), 50));
        menus.add(MetaMenu.createEruptClassMenu(RiskManagement.class, menus.get(0), 60));
        menus.add(MetaMenu.createEruptClassMenu(LoanBillManagement.class, menus.get(0), 70));// 借款单
        menus.add(MetaMenu.createEruptClassMenu(ReimbursementManagement.class, menus.get(0), 80)); // 报销单
        menus.add(MetaMenu.createEruptClassMenu(ReimbursementItemCosts.class, menus.get(0), 81)); // 报销单
        menus.add(MetaMenu.createEruptClassMenu(ReimbursementItemTravels.class, menus.get(0), 82)); // 报销单
        menus.add(MetaMenu.createEruptClassMenu(AccountingManagement.class, menus.get(0), 90));
        menus.add(MetaMenu.createEruptClassMenu(InstructionsAndMessages.class, menus.get(0), 100));




        return menus;
    }

    // 初始化方法 → 仅执行一次，标识文件位置.erupt/.${moduleName}
    public void initFun() {

    }
}
