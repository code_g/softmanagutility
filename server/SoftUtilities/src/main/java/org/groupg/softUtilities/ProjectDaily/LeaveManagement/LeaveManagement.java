package org.groupg.softUtilities.ProjectDaily.LeaveManagement;

import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.DateType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.annotation.sub_field.sub_edit.VL;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Erupt(name = "请假管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_leavemanagement_ins")    //数据库表名
@Entity
@Getter
@Setter
public class LeaveManagement extends HyperModelVo {
    /**
     * 请假开始时间
     */
    @EruptField(
            views = @View(title = "请假开始时间"),
            edit = @Edit(title = "请假开始时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date startDay;
    /**
     * 请假结束时间
     */
    @EruptField(
            views = @View(title = "请假结束时间"),
            edit = @Edit(title = "请假结束时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date endDay;

    /**
     * 请假类型
     */
    @EruptField(
            views = @View(title = "请假类型"),
            edit = @Edit(title = "请假类型", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "加班调休/行内请假")
                            , @VL(value = "0", label = "调休")
                            , @VL(value = "2", label = "事假")
                            , @VL(value = "3", label = "病假")
                            , @VL(value = "4", label = "陪产假")
                            , @VL(value = "5", label = "产假")
                            , @VL(value = "6", label = "婚假")
                            , @VL(value = "7", label = "丧假")
                    })
                    , search = @Search))
    private String leaveType = "1";

    /**
     * 请假状态
     */
    @EruptField(
            views = @View(title = "请假状态"),
            edit = @Edit(title = "请假状态", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "申请中")
                            , @VL(value = "2", label = "已确认")
                    })
                    , search = @Search))
    private String leaveStatue = "1";

    /**
     * 请假内容说明
     */
    @EruptField(
            views = @View(title = "请假内容说明"),
            edit = @Edit(title = "请假内容说明", type = EditType.TEXTAREA, search = @Search(vague = true))
    )
    private String leaveRemark = "";
    /**
     * 假期行程简要说明
     */
    @EruptField(
            views = @View(title = "假期行程简要说明"),
            edit = @Edit(title = "假期行程简要说明", type = EditType.TEXTAREA, search = @Search(vague = true))
    )
    private String itineraryDescription = "";
}
