package org.groupg.softUtilities.ProjectConfiguration.PersonInfo;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;
import xyz.erupt.upms.model.EruptUser;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;


@Erupt(name = "项目成员管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
//erupt类注解
@Table(name = "dtb_commons_personinfo_ins")    //数据库表名
@Entity
@Getter
@Setter
public class PersonInfo extends HyperModelVo {

    /*
     * 仅需将 fetchHandler 定义为 SqlChoiceFetchHandler 即可
     * fetchHandlerParams 参数值为 SQL 语句
     */
//    @EruptField(
//        views = @View(title = "用户"),
//        edit = @Edit(
//            search = @Search,
//            title = "用户",
//            type = EditType.CHOICE,
//            choiceType = @ChoiceType(
//                fetchHandler = SqlChoiceFetchHandler.class,
//                //参数一必填，表示sql语句
//                //参数二可不填，表示缓存时间，默认为3000毫秒，1.6.10及以上版本支持
//                fetchHandlerParams = {"select account as id, name from e_upms_user", "5000"}
//            ))
//    )
//    private String account;

    /*
     * 仅需将 fetchHandler 定义为 SqlChoiceFetchHandler 即可
     * fetchHandlerParams 参数值为 SQL 语句
     */
//    @EruptField(
//            views = @View(title = "所属项目"),
//            edit = @Edit(
//                    search = @Search,
//                    title = "所属项目",
//                    type = EditType.CHOICE,
//                    choiceType = @ChoiceType(
//                            fetchHandler = SqlChoiceFetchHandler.class,
//                            //参数一必填，表示sql语句
//                            //参数二可不填，表示缓存时间，默认为3000毫秒，1.6.10及以上版本支持
//                            fetchHandlerParams = {"select id, name from DTB_COMMONS_PROJECTINFO_INS", "5000"}
//                    ))
//    )
//    private Long projectID;

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_project_person_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "person_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "project_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "项目信息管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ProjectInfo> projectInfos;


    @EruptField(
            views = @View(title = "入场时间"),
            edit = @Edit(title = "入场时间",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date startDay;

    @EruptField(
            views = @View(title = "离场时间"),
            edit = @Edit(title = "离场时间",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    private Date endDay;

    @EruptField(
            views = @View(title = "性别"),
            edit = @Edit(title = "性别", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "男"),
                            @VL(value = "2", label = "女")
                    })
                    , search = @Search))
    private String gender = "1";

    @EruptField(
            views = @View(title = "学历"),
            edit = @Edit(title = "学历", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "专科"),
                            @VL(value = "2", label = "本科"),
                            @VL(value = "3", label = "硕士"),
                            @VL(value = "4", label = "博士")
                    })
                    , search = @Search))
    private String education = "2";

    @EruptField(
            views = @View(title = "年龄"),
            edit = @Edit(title = "年龄", numberType = @NumberType(min = 0))
    )
    private Integer age = 18;

    @EruptField(
            views = @View(title = "身份证号"),
            edit = @Edit(title = "身份证号", search = @Search))
    private String idCard = "";

    @EruptField(
            views = @View(title = "毕业院校"),
            edit = @Edit(title = "毕业院校", search = @Search))
    private String school = "";

    @EruptField(
        views = @View(title = "身份附件"),
        edit = @Edit(title = "身份附件", type = EditType.ATTACHMENT,
                     attachmentType = @AttachmentType)
    )
    private String attachment;


    @ManyToOne
    @EruptField(
            views = {
                    @View(title = "用户名", column = "account"),
                    @View(title = "姓名", column = "name")
            },
            edit = @Edit(title = "用户信息", desc = "注意：需要先在用户中心完成用户的新增之后才能在这里添加用户", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType()
            )
    )
    private EruptUser user;

//    @ManyToMany //多对多
//    @JoinTable(name = "DTB_COMMONS_PERSONINFO_INS_ROLES", //定义多对多中间表
//               joinColumns = @JoinColumn(name = "PERSON_ID"/* 多对多的依赖关系中间表的字段名称 在这个场景中用的是 PersonInfo 的 id 在中间表中 名字为 PERSON_ID */, referencedColumnName = "id"),
//               inverseJoinColumns = @JoinColumn(name = "ROLE_ID",/* 多对多的依赖关系中间表的被依赖的字段名称 在这个场景中用的是 EruptRole 的 id 在中间表中 名字为 ROLE_ID */ referencedColumnName = "id"))
//    @EruptField(
//        edit = @Edit(
//            title = "角色",
//            type = EditType.TAB_TABLE_REFER
//        )
//    )
//    private Set<EruptRole> eruptRoles;// 多角色的依赖


}
