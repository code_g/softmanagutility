package org.groupg.softUtilities.SoftwareEngineering.Requirements;


import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectDaily.TaskInfo.TaskInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Erupt(name = "需求变更单管理"
        , power = @Power(importable = true, export = true))        //erupt类注解
@Table(name = "dtb_commons_versioning_ins")    //数据库表名
@Entity
@Getter
@Setter
public class Requirements extends HyperModelVo {

    @EruptField(
            views = @View(title = "需求名称"),
            edit = @Edit(title = "需求名称", search = @Search)
    )
    private String name;

    @EruptField(
            views = @View(title = "需求提出部门"),
            edit = @Edit(title = "需求提出部门", search = @Search)
    )
    private String org;

    @EruptField(
            views = @View(title = "需求提出人"),
            edit = @Edit(title = "需求提出人", search = @Search)
    )
    private String author;

    @EruptField(
            views = @View(title = "计划开始时间"),
            edit = @Edit(title = "计划开始时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "startDay")
    private Date startDay;

    @EruptField(
            views = @View(title = "计划结束时间"),
            edit = @Edit(title = "计划结束时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "endDay")
    private Date endDay;

    /**
     * 需求状态
     */
    @EruptField(
            views = @View(title = "需求状态"),
            edit = @Edit(title = "需求状态", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "排队中")
                            , @VL(value = "2", label = "正在进行")
                            , @VL(value = "3", label = "已完成")
                            , @VL(value = "3", label = "已结算")
                    })
                    , search = @Search))
    private String taskStatue = "1";

    @EruptField(
            views = @View(title = "计划工作量"),
            edit = @Edit(title = "计划工作量(人天)", type = EditType.NUMBER)
    )
    private Double workload;

    @EruptField(
            views = @View(title = "实际开始时间"),
            edit = @Edit(title = "实际开始时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "startDayOver")
    private Date startDayOver;

    @EruptField(
            views = @View(title = "实际结束时间"),
            edit = @Edit(title = "实际结束时间",
                    dateType = @DateType(type = DateType.Type.DATE_TIME, pickerMode = DateType.PickerMode.ALL) //选择历史时间
                    , search = @Search))
    @Column(name = "endDayOver")
    private Date endDayOver;

    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "需求详细说明", type = ViewType.HTML, export = false),
            edit = @Edit(title = "需求详细说明", desc = "可以把需求的关注点、重要的点写这里",
                     type = EditType.HTML_EDITOR,
                     htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.UEDITOR))
    )
    private String content;


    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_task_requirements_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "requirements_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "task_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "关联任务",desc = "工作任务管理中的数据，先加任务，再关联需求",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<TaskInfo> taskInfos;

    @EruptField(
        views = @View(title = "需求附件"),
        edit = @Edit(title = "需求附件",desc = "理论上可以上传100个文件，但是还请不要作妖的好。", type = EditType.ATTACHMENT,
                     attachmentType = @AttachmentType(maxLimit=100))
    )
    private String attachment;
}
