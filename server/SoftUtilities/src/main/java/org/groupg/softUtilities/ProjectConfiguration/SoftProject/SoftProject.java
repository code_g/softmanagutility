package org.groupg.softUtilities.ProjectConfiguration.SoftProject;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.annotation.sub_field.sub_edit.VL;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Set;

@Erupt(name = "软件项目信息管理"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
//erupt类注解
@Table(name = "dtb_commons_softproject_ins")    //数据库表名
@Entity
@Getter
@Setter
public class SoftProject extends HyperModelVo {

    /**
     * 软件项目名称
     */
    @EruptField(
            views = @View(title = "软件项目名称"),
            edit = @Edit(title = "软件项目名称", search = @Search)
    )
    private String name;

    @EruptField(
            views = @View(title = "软件类型"),
            edit = @Edit(title = "软件类型", type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "1", label = "单体架构项目"),
                            @VL(value = "2", label = "前端项目"),
                            @VL(value = "3", label = "后端项目"),
                            @VL(value = "4", label = "批次项目"),
                            @VL(value = "5", label = "其他项目"),
                            @VL(value = "6", label = "计算模块"),
                            @VL(value = "7", label = "工作流模块"),
                            @VL(value = "8", label = "调度模块"),
                            @VL(value = "9", label = "核心模块")
                    })
                    , search = @Search))
    private Integer type = 1; // 默认为单体架构项目

    /**
     * 项目路径
     */
    @EruptField(
            views = @View(title = "项目路径"),
            edit = @Edit(title = "项目路径", search = @Search)
    )
    private String path;

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_project_softproject_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "softproject_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "project_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "项目信息管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ProjectInfo> projectInfos;

    /**
     * 说明
     */
    @EruptField(
            views = @View(title = "说明"),
            edit = @Edit(title = "说明", type = EditType.TEXTAREA, search = @Search)
    )
    private String remark;
}
