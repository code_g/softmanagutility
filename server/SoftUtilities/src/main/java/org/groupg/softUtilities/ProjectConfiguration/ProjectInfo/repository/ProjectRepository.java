package org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.repository;


import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectRepository extends JpaRepository<ProjectInfo, Long> {
    List<ProjectInfo> findAllByProjectManager(String projectManager);
}
