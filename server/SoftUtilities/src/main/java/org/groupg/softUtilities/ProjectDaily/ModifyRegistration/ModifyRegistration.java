package org.groupg.softUtilities.ProjectDaily.ModifyRegistration;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.SoftProject.SoftProject;
import org.groupg.softUtilities.SoftwareEngineering.CodeEnvironmental.CodeEnvironmental;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.toolkit.handler.SqlChoiceFetchHandler;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Erupt(name = "变更管理"
        , power = @Power(importable = true, export = true))        //erupt类注解
@Table(name = "dtb_commons_modifyregistration_ins")    //数据库表名
@Entity
@Getter
@Setter
public class ModifyRegistration extends HyperModelVo {

    /**
     * 登记内容简介
     */
    @EruptField(
            views = @View(title = "登记内容简介"),
            edit = @Edit(title = "登记内容简介", search = @Search)
    )
    private String name;

//    @EruptField(
//            views = @View(title = "通过SQL获取下拉列表"),
//            edit = @Edit(
//                    search = @Search,
//                    title = "通过SQL获取下拉列表",
//                    type = EditType.CHOICE,
//                    choiceType = @ChoiceType(
//                            fetchHandler = SqlChoiceFetchHandler.class,
//                            //参数一必填，表示sql语句
//                            //参数二可不填，表示缓存时间，默认为3000毫秒，1.6.10及以上版本支持
//                            fetchHandlerParams = {"select id, name from DTB_COMMONS_SoftProject_INS", "5000"}
//                    ))
//    )
//    private Long softwareId;

    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_softproject_modifyregistration_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "modifyregistration_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "softproject_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "软件项目信息管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<SoftProject> softProjects;


    @ManyToOne //多对多
    @JoinTable(name = "dtb_commons_code_modifyregistration_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "modifyregistration_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "code_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属代码环境",desc = "代码版本环境管理中的数据",
            type = EditType.REFERENCE_TREE
        )
    )
    private CodeEnvironmental codeEnvironmental;


    @EruptField(
            views = @View(title = "变更开始标记"),
            edit = @Edit(title = "变更开始标记", desc = "如果是 git 的话就是 提交的ID", search = @Search)
    )
    private String startID;

    @EruptField(
            views = @View(title = "变更结束标记"),
            edit = @Edit(title = "变更结束标记", desc = "如果是 git 的话就是 提交的ID", search = @Search)
    )
    private String endID;

    @EruptField(
            views = @View(title = "变更人"),
            edit = @Edit(
                    search = @Search,
                    title = "变更人",
                    type = EditType.CHOICE,
                    choiceType = @ChoiceType(
                            fetchHandler = SqlChoiceFetchHandler.class,
                            //参数一必填，表示sql语句
                            //参数二可不填，表示缓存时间，默认为3000毫秒，1.6.10及以上版本支持
                            fetchHandlerParams = {"select id, name from e_upms_user", "5000"}
                    ))
    )
    private Long personID;

    /**
     * 变更状态
     */
    @EruptField(
            views = @View(title = "变更状态"),
            edit = @Edit(title = "变更状态", search = @Search, type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "0", label = "开发环境")
                            , @VL(value = "1", label = "测试环境")
                            , @VL(value = "2", label = "生产环境") /* 后续调整为关联的 */
                    })
            )
    )
    private String modifyStatue;

    /**
     * 变更类型
     */
    @EruptField(
            views = @View(title = "变更类型"),
            edit = @Edit(title = "变更类型", search = @Search, type = EditType.CHOICE,
                    choiceType = @ChoiceType(vl = {
                            @VL(value = "00", label = "SQL")
                            , @VL(value = "01", label = "代码文件")
                            , @VL(value = "02", label = "batch脚本") /* 后续调整为关联的 */
                            , @VL(value = "03", label = "存储过程")
                            , @VL(value = "04", label = "产品-快速开发平台-功能")
                            , @VL(value = "05", label = "产品-快速开发平台-逻辑组")
                            , @VL(value = "06", label = "产品-快速开发平台-单逻辑")
                            , @VL(value = "07", label = "产品-流程图")
                            , @VL(value = "08", label = "产品-规则图")
                            , @VL(value = "09", label = "菜单配置")
                            , @VL(value = "10", label = "权限配置")
                            , @VL(value = "11", label = "批次步骤配置")

                    })
            )
    )
    private String modifyType;

    @EruptField(
            views = @View(title = "变更时间"),
            edit = @Edit(title = "变更时间",
                    dateType = @DateType(pickerMode = DateType.PickerMode.ALL, type = DateType.Type.DATE_TIME) //选择历史时间
                    , search = @Search))
    private Date datetime;
    /**
     * 登记详细说明
     */
    @EruptField(
            views = @View(title = "登记详细说明"),
            edit = @Edit(title = "登记详细说明 - 只是说明，不影响任何任何过程", type = EditType.TEXTAREA, search = @Search)
    )
    private String remark;


    /**
     * 变更内容
     */
    @Lob
    @EruptField(
            views = @View(title = "变更内容"),
            edit = @Edit(title = "变更内容",desc = "具体的变更内容", type = EditType.TEXTAREA, search = @Search)
    )
    private String changeCode;

    @Lob
    @EruptField(
            views = @View(title = "登记SQL" , type = ViewType.CODE),
            edit = @Edit(title = "登记SQL - 非SQL不要登记在这里",
                    type = EditType.CODE_EDITOR,
                    codeEditType = @CodeEditorType(language = "SQL"))
    )
    private String sqlCode;

        @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "集成运行脚本", type = ViewType.CODE),
            edit = @Edit(title = "集成运行脚本(shell)", desc = "注意，此处登记的是shell脚本",
                    type = EditType.CODE_EDITOR,
                    codeEditType = @CodeEditorType(language = "shell"))
    )
    private String ciContent;

    @Lob  //富文本编辑器所产生的文本量较大，所以设置为长字符串类型在数据库中存储
    @EruptField(
            views = @View(title = "部署运行脚本", type = ViewType.CODE),
            edit = @Edit(title = "部署运行脚本(shell)", desc = "注意，此处登记的是shell脚本",
                    type = EditType.CODE_EDITOR,
                    codeEditType = @CodeEditorType(language = "shell"))
    )
    private String cdContent;

}
