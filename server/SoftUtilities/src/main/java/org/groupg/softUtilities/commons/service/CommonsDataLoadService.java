package org.groupg.softUtilities.commons.service;

import lombok.extern.slf4j.Slf4j;
import org.groupg.softUtilities.commons.utils.RoleUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import xyz.erupt.core.util.MD5Util;
import xyz.erupt.core.util.ProjectUtil;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.upms.model.EruptUser;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Date;

@Service
@Order
@Slf4j
public class CommonsDataLoadService implements CommandLineRunner {
    public static final String DEFAULT_ACCOUNT = "sundata";
    @Resource
    private EruptDao eruptDao;
    @Resource
    private RoleUtils roleUtils;
    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Transactional
    @Override
    public void run(String... args) throws Exception {
        new ProjectUtil().projectStartLoaded("SUNDATA-CommonsConfigurationModule", first -> {
            if (first) {
                //用户
                if (eruptDao.queryEntityList(EruptUser.class, "isAdmin = true and account != 'erupt'").size() <= 0) {
                    EruptUser eruptUser = new EruptUser();
                    eruptUser.setIsAdmin(true);
                    eruptUser.setIsMd5(true);
                    eruptUser.setStatus(true);
                    eruptUser.setCreateTime(new Date());
                    eruptUser.setAccount(DEFAULT_ACCOUNT);
                    eruptUser.setPassword(MD5Util.digest(DEFAULT_ACCOUNT));
                    eruptUser.setName(DEFAULT_ACCOUNT);
                    eruptDao.persistIfNotExist(EruptUser.class, eruptUser, "account", eruptUser.getAccount());
                }
                // 角色

            }
        });
    }
}
