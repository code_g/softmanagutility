package org.groupg.softUtilities.SoftwareEngineering.Environmental;


import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Table;

@Erupt(name = "关键路径管理" ,desc = "针对特殊情况对路径进行说明，该路径可被用于关键环境配置以及脚本过程中调用"
        , power = @Power(importable = true, export = true))        //erupt类注解
@Table(name = "dtb_commons_environmental_path_ins")    //数据库表名
@Entity
@Getter
@Setter
public class CriticalPath extends HyperModelVo {


    @EruptField(
            views = @View(title = "路径代号"),
            edit = @Edit(title = "路径代号",desc = "该字段可以在登记变更的脚本中被通过 ${criticalPath[路径代号]} 调用，结果为属性 “绝对路径”",notNull = true, search = @Search)
    )
    private String code;

    @EruptField(
            views = @View(title = "路径名称"),
            edit = @Edit(title = "路径名称", search = @Search)
    )
    private String name;

    @EruptField(
            views = @View(title = "绝对路径"),
            edit = @Edit(title = "绝对路径", search = @Search)
    )
    private String path;

    @EruptField(
            views = @View(title = "路径说明"),
            edit = @Edit(title = "路径说明" ,desc = "" ,type = EditType.TEXTAREA, search = @Search)
    )
    private String remark;
}
