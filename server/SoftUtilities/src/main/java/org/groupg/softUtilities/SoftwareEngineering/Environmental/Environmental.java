package org.groupg.softUtilities.SoftwareEngineering.Environmental;

import lombok.Getter;
import lombok.Setter;
import org.groupg.softUtilities.ProjectConfiguration.ProjectInfo.ProjectInfo;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.*;
import java.util.Set;

@Erupt(name = "项目运行环境管理"
        , power = @Power(importable = true, export = true))        //erupt类注解
@Table(name = "dtb_commons_environmental_ins")    //数据库表名
@Entity
@Getter
@Setter
public class Environmental extends HyperModelVo {


    @ManyToMany //多对多
    @JoinTable(name = "dtb_commons_project_environmental_res", //定义多对多中间表
               joinColumns = @JoinColumn(name = "environmental_id"/* 本表的数据字段 */, referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "project_id"/* 对象表的数据字段 */, referencedColumnName = "id"))
    @EruptField(
        edit = @Edit(
            title = "所属项目",desc = "项目信息管理中的数据",
            type = EditType.TAB_TABLE_REFER
        )
    )
    private Set<ProjectInfo> projectInfos;

    @EruptField(
            views = @View(title = "环境组"),
            edit = @Edit(title = "环境组", search = @Search)
    )
    private String env_group;
    @EruptField(
            views = @View(title = "环境名称"),
            edit = @Edit(title = "环境名称", search = @Search)
    )
    private String name;

    @EruptField(
            views = @View(title = "环境IP"),
            edit = @Edit(title = "环境IP", search = @Search)
    )
    private String ipaddress;

    @EruptField(
            views = @View(title = "SSH端口"),
            edit = @Edit(title = "SSH端口", search = @Search)
    )
    private Integer port = 22;

    @EruptField(
            views = @View(title = "默认用户名称"),
            edit = @Edit(title = "默认用户名称", search = @Search)
    )
    private String username;

    @EruptField(
            views = @View(title = "默认用户密码"),
            edit = @Edit(title = "默认用户密码")
    )
    private String password;

//    @EruptField(
//            views = @View(title = "负责人"),
//            edit = @Edit(
//                    search = @Search,
//                    title = "负责人",
//                    type = EditType.CHOICE,
//                    choiceType = @ChoiceType(
//                            fetchHandler = SqlChoiceFetchHandler.class,
//                            //参数一必填，表示sql语句
//                            //参数二可不填，表示缓存时间，默认为3000毫秒，1.6.10及以上版本支持
//                            fetchHandlerParams = {"select account as id, name from e_upms_user", "5000"}
//                    ))
//    )
//    private String head;


    @ManyToMany //多对多
    @JoinTable(name = "environmental_user", //定义多对多中间表
            joinColumns = @JoinColumn(name = "env_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "env_user_id", referencedColumnName = "id"))
    @EruptField(
            edit = @Edit(
                    title = "系统用户",desc = "运行环境用户管理中的数据",
                    type = EditType.TAB_TABLE_REFER
            )
    )
    private Set<EnvUser> envUsers; //Table对象定义如下👇

    @ManyToMany //多对多
    @JoinTable(name = "environmental_criticalpath", //定义多对多中间表
            joinColumns = @JoinColumn(name = "env_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "env_path_id", referencedColumnName = "id"))
    @EruptField(
            edit = @Edit(
                    title = "关键路径",desc = "关键路径管理中的数据",
                    type = EditType.TAB_TABLE_REFER
            )
    )
    private Set<CriticalPath> criticalPaths;

    @Lob
    @EruptField(
            views = @View(title = "说明"),
            edit = @Edit(title = "说明", type = EditType.TEXTAREA)
    )
    private String remark;

}
