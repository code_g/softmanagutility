package org.groupg.softUtilities.ProjectConfiguration.ProjectInfo;

import org.springframework.stereotype.Component;
import xyz.erupt.annotation.fun.DataProxy;
import xyz.erupt.annotation.model.Column;
import xyz.erupt.annotation.model.Row;
import xyz.erupt.annotation.query.Condition;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义行的处理
 */
@Component
public class ExtraRowHandler implements DataProxy<ProjectInfo> {

    /**
     * @param conditions 参数对象的含义是每个查询条件作为一个对象，list中包含所有的查询条件，
     * @return
     */
    @Override
    public List<Row> extraRow(List<Condition> conditions) {
        //行对象
        List<Row> rows = new ArrayList<>();
        //列对象
        List<Column> columns = new ArrayList<>();

        columns.add(Column.builder().value("自定义行").colspan(1).build());
        columns.add(Column.builder().value(100 + "").colspan(6).className("text-red").build());

        rows.add(Row.builder().columns(columns).build());
        return rows;
    }

}