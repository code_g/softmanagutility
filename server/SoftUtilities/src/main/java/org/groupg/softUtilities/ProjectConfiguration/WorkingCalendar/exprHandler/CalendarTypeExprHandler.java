package org.groupg.softUtilities.ProjectConfiguration.WorkingCalendar.exprHandler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import xyz.erupt.annotation.expr.ExprBool;
import xyz.erupt.annotation.sub_field.Readonly;

import java.util.Arrays;

@Component
@Slf4j
public class CalendarTypeExprHandler implements ExprBool.ExprHandler, Readonly.ReadonlyHandler {


    /**
     * @param expr   表达式
     * @param params 注解参数
     * @return 程序处理后的表达式
     */
    @Override
    public boolean handler(boolean expr, String[] params) {

        log.debug(Arrays.toString(params));
        return false;
    }

    /**
     * @param add
     * @param params
     * @return
     */
    @Override
    public boolean add(boolean add, String[] params) {
        return false;
    }

    /**
     * @param edit
     * @param params
     * @return
     */
    @Override
    public boolean edit(boolean edit, String[] params) {
        return false;
    }
}
