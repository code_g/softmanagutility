package org.groupg.softUtilities.ProjectDaily.ReimbursementManagement;

import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.NumberType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Table;


@Erupt(name = "报销单项目-费用项目清单"
        , power = @Power(importable = true, export = true) /*, dataProxy = ExtraRowHandler.class 自定义行的练习，但是项目基本信息不需要*/)
@Table(name = "dtb_commons_reimbursement_items_cost_ins")    //数据库表名
@Entity
@Getter
@Setter
public class ReimbursementItemCosts extends HyperModelVo {


    /**
     * 费用项目
     */
    @EruptField(
            views = @View(title = "费用项目"),
            edit = @Edit(title = "费用项目", search = @Search(vague = true))
    )
    private String expenseName;

    /**
     * 费用类别
     */
    @EruptField(
            views = @View(title = "费用类别"),
            edit = @Edit(title = "费用类别", search = @Search(vague = true))
    )
    private String expenseType;

    /**
     * 项目金额
     */
    @EruptField(
        edit = @Edit(title = "项目金额", numberType = @NumberType)
    )
    private Double expenseMount;
}
