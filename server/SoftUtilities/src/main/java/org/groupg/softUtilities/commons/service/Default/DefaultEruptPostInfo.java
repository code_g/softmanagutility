package org.groupg.softUtilities.commons.service.Default;

import xyz.erupt.upms.model.EruptPost;

import java.util.ArrayList;
import java.util.List;

public class DefaultEruptPostInfo {
    private static final List<EruptPost> POSTLIST = new ArrayList<>();

    public synchronized static List<EruptPost> getDefaultPostList(){
        if (POSTLIST.size() == 0){
            initPOSTLIST();
        }
        return POSTLIST;
    }

    static void initPOSTLIST(){
        POSTLIST.clear();
        EruptPost post ;
        post = new EruptPost();
        post.setCode("000");
        post.setName("待定");
        post.setWeight(0);
        POSTLIST.add(post);
        post.setCode("010");
        post.setName("初级");
        post.setWeight(10);
        POSTLIST.add(post);
        post = new EruptPost();
        post.setCode("020");
        post.setName("中级");
        post.setWeight(20);
        POSTLIST.add(post);
        post = new EruptPost();
        post.setCode("030");
        post.setName("高级");
        post.setWeight(30);
        POSTLIST.add(post);
        post = new EruptPost();
        post.setCode("040");
        post.setName("特级");
        post.setWeight(40);
        POSTLIST.add(post);
        post = new EruptPost();
        post.setCode("050");
        post.setName("特聘");
        post.setWeight(50);
        POSTLIST.add(post);
    }
}
