package org.groupg.systemcore.SystemCoreMain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SystemCoreMainApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemCoreMainApplication.class, args);
	}

}
