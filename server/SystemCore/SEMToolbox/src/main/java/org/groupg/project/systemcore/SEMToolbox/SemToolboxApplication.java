package org.groupg.project.systemcore.SEMToolbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemToolboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemToolboxApplication.class, args);
	}

}
