import { defineConfig } from 'dumi';

export default defineConfig({
  title: '驻场团队管理工具',
  mode: 'doc',
  // more config: https://d.umijs.org/config
  locales:[['zh-CN','中文']],

});
